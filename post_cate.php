<?php
include "header.php";
if (isset($_GET['id'])) {
	$id = $_GET['id'];
}
?>
<!-- Blog -->
<link rel="stylesheet" type="text/css" href="styles/blog.css">

<div class="blog" style="height: 1000px;margin-top: 50px;">
	<div class="container">
		<div class="row">
			<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
				<?php
				foreach (getData("SELECT * FROM post WHERE id_cate = '$id'") as $post) { ?>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="border: 1px solid #cdcdcd;padding: 15px;margin-bottom: 20px;">

						<div class="row">
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
								<img src="public/img/<?= $post['img'] ?>" width="200px" height="200px" alt="">
							</div>

							<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
								<a href="details_post.php?id=<?= $post['id'] ?>"><?= $post['title'] ?></a>
								<p><?= substr($post['content'], 0, 200) ?>...</p>
							</div>

						</div>

					</div>
				<?php

				}

				?>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
				<?php
				foreach (getData("SELECT * FROM category") as $cate) { ?>
					<div style="height: 50px; border: 1px solid #cdcdcd;padding: 15px;">
						<a href="post_cate.php?id=<?= $cate['id'] ?>"><?= $cate['name'] ?></a>
					</div>
				<?php
				}

				?>
			</div>


		</div>
		<!-- <div class="row">

			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="border: 1px solid #cdcdcd;padding: 10px;">
				<?php
				foreach (getData("SELECT * FROM category") as $cate) { ?>
					<div style="height: 50px;border-bottom: 1px solid #cdcdcd;">
						<a href="post_cate.php?id=<?= $cate['id'] ?>"><?= $cate['name'] ?></a>
					</div>
				<?php
				}

				?>
			</div>

			<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
				<?php
				foreach (getData("SELECT * FROM post WHERE id_cate = '$id'") as $post) { ?>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="border: 1px solid #cdcdcd;padding: 15px;margin-bottom: 20px;height: 250px;overflow: hidden;">

						<div class="row">
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
								<img src="public/img/<?= $post['img'] ?>" width="200px" height="200px" alt="">
							</div>

							<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
								<a href="details_post.php?id=<?= $post['id'] ?>"><?= $post['title'] ?></a>
								<p><?= $post['content'] ?></p>
							</div>

						</div>

					</div>
				<?php

				}

				?>
			</div>


		</div> -->

	</div>
</div>

<?php
include "footer.php";
?>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/blog.js"></script>