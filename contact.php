<?php
include "header.php";
if (isset($_POST['submit'])) {
	$name = $_POST['name'];
	$phone = $_POST['phone'];
	$details = $_POST['details'];
	$status =0;
	action("INSERT INTO tuvan(name,phone,content,status) VALUES ('$name','$phone','$details','$status')");
	$error = "Gửi thành công";
}
?>
<link rel="stylesheet" type="text/css" href="styles/contact.css">


<div class="contact_info_container" style="margin: 100px 0px;">
	<div class="container">
		<div class="row">

			<!-- Contact Form -->

			<div class="col-lg-6">
				<?php
				if (isset($error)) { ?>
					<p class="alert alert-danger"><?= $error ?></p>
				<?php

				}
				?>
				<div class="contact_form">
					<div class="contact_info_title">Liên hệ</div>
					<form method="POST" class="comment_form">
						<div>
							<div class="form_title">Tên</div>
							<input type="text" class="comment_input" name="name" required="required">
						</div>
						<div>
							<div class="form_title">Số điện thoại</div>
							<input type="text" class="comment_input" name="phone" required="required">
						</div>
						<div>
							<div class="form_title">Chi tiết</div>
							<textarea class="comment_input comment_textarea" name="details" required="required"></textarea>
						</div>
						<div>
							<button type="submit" name="submit" class="comment_button trans_200">Gửi</button>
						</div>
					</form>
				</div>
			</div>

			<!-- Contact Info -->
			<div class="col-lg-6">
				<div class="contact_info">
					<div class="contact_info_title">Liên hệ với chúng tôi</div>
					<div class="contact_info_text">
						<p>
							“TÂM AN LÀ CON NGOAN ” - Mầm non Ánh Mai Sáng
							sẽ không ngừng nỗ lực để đồng hành và giúp con định hình và chinh phục những nguồn
							tri thức cơ bản nhất trong quá trình phát triển đầu đời.
						</p>
					</div>
					<div class="contact_info_location">
						<div class="contact_info_location_title">Hà Nội</div>
						<ul class="location_list">
							<li>Biệt thự 1 A1 - Dốc 931 Đê La Thành</li>
							<li>(024)37759861/ 098699861</li>
							<li>ams@gmail.com</li>
						</ul>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
</div>

<!-- Newsletter -->

<?php
include "footer.php";
?>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>
<script src="plugins/marker_with_label/marker_with_label.js"></script>
<script src="js/contact.js"></script>