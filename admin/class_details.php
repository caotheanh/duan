<?php
include "header.php";
// include "../function.php";
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $stt = 0;
} else {
    header("Location:index.php");
}
?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <?php foreach (getData("SELECT * FROM class WHERE id ='$id'") as $row) { ?>
                    <h1 class="page-header">Quản trị lớp <?= $row['name'] ?></h1>
                <?php

                } ?>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Khối</th>
                                <th>Tên lớp</th>
                                <th>Sĩ số</th>
                                <th>Giáo viên</th>
                                <th>Xem chi tiết</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach (getData("SELECT * FROM class WHERE id = '$id'") as $class) {
                                $totalStudent = total("SELECT COUNT(*) FROM student WHERE id_class =" . $class['id']);
                                foreach (getData("SELECT * FROM group_class WHERE id =" . $class['id_gr']) as $gr_class) { ?>
                                    <tr>
                                        <td><?= $gr_class['name'] ?></td>
                                        <td><?= $class['name'] ?></td>
                                        <td><?=$totalStudent ?></td>
                                        <td>
                                            <?php
                                                foreach(getData("SELECT * FROM teacher WHERE id_class=".$class['id']) as $teacher){?>
                                                 <a href="teacher_details.php?id_teacher=<?=$teacher['id'] ?>"><?=$teacher['name'] ?></a> <br>
                                                <?php
                                                    

                                                }
                                            ?>
                                        </td>
                                        <td><a href="student.php?id=<?=$class['id']?>">Danh sách học sinh</a></td>
                                    </tr>
                            <?php

                                }
                            }
                            ?>
                            <!-- <tr>
                               <td>Khối 2 tuổi</td>
                               <td>2A-T1</td>
                               <td>30</td>
                               <td>Anh</td>
                           </tr> -->
                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>
</div>
</div>
<script src="../public/js/jquery.min.js"></script>
<script src="../public/js/bootstrap.min.js"></script>
<script src="../public/js/metisMenu.min.js"></script>
<script src="../public/js/startmin.js"></script>
</body>

</html>