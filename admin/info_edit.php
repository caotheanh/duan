<?php
include "header.php";
// include "../function.php";
if (isset($_GET['id'])) {
    $info = $_GET['id'];
    foreach (getData("SELECT * FROM info WHERE id = '$info'") as $row) {
        $phone = isset($row['phone']) ? $row['phone'] : '';
        $email = isset($row['email']) ? $row['email'] : '';
        $address = isset($row['address']) ? $row['address'] : '';
        $why = isset($row['why']) ? $row['why'] : '';
    }
    if (isset($_POST['submit'])) {
       $phone_new = $_POST['phone'];
       $email_new = $_POST['email'];
       $address_new = $_POST['address'];
       $why_new = $_POST['why'];
       action("UPDATE info SET phone = '$phone_new',email = '$email_new',address='$address_new',why='$why_new'");
        $error = "Cập nhật thành công";
    }
}
?>
<style>
    form input {
        width: 100%;
        height: 30px;
        border: 1px solid #cdcdcd;
        border-radius: 5px;
        margin: 10px 0;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Cập nhật thông tin giáo viên</h1>
            </div>
        </div>
        <?php
        if (isset($error)) { ?>
            <p class="alert alert-danger"><?= $error ?></p>
        <?php


        }
        ?>
        <form method="post">
            <div class="row">

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="phone">Phone</label> <br>
                    <input type="text" name="phone" value="<?= $phone ?>"> <br>
                    <label for="email">Email</label> <br>
                    <input type="text" name="email" value="<?= $email ?>"> <br>
                    <label for="address">Địa chỉ</label> <br>
                    <input type="text" name="address" value="<?= $address ?>"> <br>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="why">Lý do</label>
                    <textarea name="why" id="" cols="30" rows="10" class="form-control"><?=$why ?></textarea>
                </div>
                <button type="submit" name="submit" class="btn btn-danger" style="margin-top: 10px;">Cập nhật</button>
                <a href="info.php" class="btn btn-primary" style="margin-top: 10px;">Quay lại</a>
        </form>
    </div>

</div>
</div>
<script src="../public/js/jquery.min.js"></script>
<script>
    function changeImg(input) {
        //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            //Sự kiện file đã được load vào website
            reader.onload = function(e) {
                //Thay đổi đường dẫn ảnh
                $('#avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function() {
        $('#avatar').click(function() {
            $('#img').click();
        });
    });
</script>

<script>
    function quay_lai_trang_truoc() {
        history.back();
    }
</script>
<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>

<script src="../public/js/startmin.js"></script>


</body>

</html>