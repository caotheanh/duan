<?php require_once "header.php" ?>
<?php
if (isset($_SESSION['parent'])) {
    $phone = $_SESSION['parent'];
    foreach (getData("SELECT * FROM parents WHERE phone_father = '$phone' OR phone_mother= '$phone'") as $parent) {
        $id = $parent['id'];
    }
}

?>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center" style="margin-top: 20px;">
                <h3 class="text-themecolor m-b-0 m-t-0">Thông tin học sinh</h3>
            </div>

        </div>
        <div class="row">
            <!-- Column -->
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <?php
                    if (isset($id)) {
                        foreach (getData("SELECT * FROM student WHERE id_ph='$id'") as $student) { ?>
                            <div class="card-block">
                                <div class="m-t-30" style="text-align: center;border-top:1px solid #cdcdcd ;"> <img src="../public/img/student/<?= $student['img'] ?>" class="img-circle" width="150" />
                                    <h4 class="card-title m-t-10"><?= $student['name'] ?></h4>

                                </div>
                            </div>
                            <div>
                                <hr>
                            </div>
                            <div class="card-block">
                                <div class="row">

                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <small class="text-muted p-t-30 db">Ngày sinh</small>
                                        <h6><?= $student['birthday'] ?></h6>

                                        <small class="text-muted p-t-30 db">Địa chỉ</small>
                                        <h6><?= $student['address'] ?></h6>
                                        <?php
                                                foreach (getData("SELECT * FROM parents WHERE id=" . $student['id_ph']) as $parent) { ?>
                                            <small class="text-muted p-t-30 db">Họ tên bố</small>
                                            <h6> <?= $parent['name_father'] ?></h6>
                                            <small class="text-muted p-t-30 db">Họ tên mẹ</small>
                                            <h6> <?= $parent['name_mother'] ?></h6>
                                        <?php
                                                }
                                                ?>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <?php
                                                foreach (getData("SELECT * FROM class WHERE id=" . $student['id_class']) as $class) { ?>
                                            <small class="text-muted p-t-30 db">Lớp</small>
                                            <h6><?= $class['name'] ?></h6>
                                        <?php

                                                }
                                                ?>

                                        <small class="text-muted p-t-30 db">Ngày bắt đầu đi học</small>
                                        <h6><?= $student['date_start_study'] ?></h6>
                                        <small class="text-muted p-t-30 db">Giới tính</small>
                                        <h6><?= $student['sex'] == 1 ? 'Nam' : 'Nữ' ?></h6>
                                    </div>

                                </div>
                            </div>
                    <?php

                        }
                    }

                    ?>

                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">

                    <!-- Tab panes -->


                </div>
            </div>
            <!-- Column -->
        </div>

    </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>

</body>

</html>