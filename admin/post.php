<?php
include "header.php";
if (isset($_GET['id_post'])) {
    $id = $_GET['id_post'];
    action("DELETE FROM post WHERE id = '$id'");
}
?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Quản trị bài viết</h1>
            </div>
        </div>
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <a href="post_add.php" class="btn btn-primary">Thêm mới</a>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Stt</th>
                            <th>Hình ảnh</th>
                            <th>Tiêu đề</th>
                           
                            <th>Người tạo</th>
                            <th>Ngày tạo</th>
                            <th>Danh mục</th>
                            <th>Quản trị</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $stt = 0;
                        foreach (getData("SELECT * FROM post") as $post) {
                            $id_admin = $post['id_admin'];
                            $id_cate = $post['id_cate'];
                            ?>
                            <tr>
                                <td><?= $stt += 1 ?></td>
                                <td><img width="150px" height="150px" src="../public/img/<?= $post['img'] ?>" alt=""></td>
                                <td><?= $post['title'] ?></td>
                               
                                <td>
                                    <?php
                                        foreach (getData("SELECT * FROM admin WHERE id = '$id_admin'") as $admin) { ?>
                                        <?= $admin['name'] ?>
                                    <?php

                                        }
                                        ?>
                                </td>
                                <td><?= $post['date_add'] ?></td>
                                <td>
                                <?php
                                        foreach (getData("SELECT * FROM category WHERE id = '$id_cate'") as $cate) { ?>
                                        <?= $cate['name'] ?>
                                    <?php

                                        }
                                        ?>

                                </td>
                                <td>
                                    <a href="post_edit.php?id_post=<?= $post['id'] ?>" class="btn btn-primary">Sửa</a>
                                    <a href="post.php?id_post=<?= $post['id'] ?>" class="btn btn-danger" onclick="return confirm('Delete?')">Xóa</a>
                                </td>
                            </tr>
                        <?php

                        }
                        ?>

                    </tbody>
                </table>

            </div>

        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>
<script src="../public/js/startmin.js"></script>

</body>

</html>