<?php
include "header.php";

if (isset($_POST['submit'])) {
    $name = $_POST['name'];
    $birthday = $_POST['birthday'];
    $sex = $_POST['sex'];
    $phone = $_POST['phone'];
    $edu = $_POST['edu'];
    $email = $_POST['email'];
    //check mail and phone


    $password = md5($_POST['password']);
    $address = $_POST['address'];
    $start_work = Date('Y-m-d');
    $img = $_FILES['img'];
    $maxSize = 800000;
    $upload = true;
    $dir = "../public/img/teacher/";
    $target_file = $dir . basename($img['name']);
    $type = pathinfo($target_file, PATHINFO_EXTENSION);
    $allowtypes = array('jpg', 'png', 'jpeg');
    $check_phone = "SELECT * FROM teacher WHERE phone= '$phone'";
    $check_email = "SELECT * FROM teacher WHERE email= '$email'";
    $cout_email = $conn->prepare($check_email);
    $cout_email->execute();
    $cout_phone = $conn->prepare($check_phone);
    $cout_phone->execute();
    if (
        empty($name) || empty($birthday) || empty($sex) ||
        empty($phone) || empty($edu) || empty($email) || empty($password) || empty($address) ||
        empty($start_work || empty($img))
    ) {
        $error = "Vui lòng điền đầy đủ thông tin";
    } elseif ($cout_phone->rowCount() > 0) {
        $error = "Số điện thoại này đã có người sử dụng. Vui lòng chọn Số khác khác! ";
    } elseif ($cout_email->rowCount() > 0) {
        $error = "Email này đã có người sử dụng. Vui lòng chọn Email khác! ";
    } elseif ($img["size"] > $maxSize) {
        $error = "File ảnh quá lớn. Vui lòng chọn ảnh khác";
        $upload = false;
    } elseif (!in_array($type, $allowtypes)) {
        $error = "Chỉ được upload các định dạng JPG, PNG, JPEG";
        $upload = false;
    } else {
        $imgname = uniqid() . "-" . $img['name'];
        if (move_uploaded_file($img['tmp_name'], $dir . $imgname)) { }

        $sql = "INSERT INTO teacher_vp (name,birthday,sex,phone,education,img,password,address,email,start_work) VALUES 
        ('$name','$birthday','$sex','$phone','$edu','$imgname','$password','$address','$email','$start_work')";
        action($sql);
        $error = "Thêm mới thành công";
    }
}
?>
<style>
    form input {
        width: 100%;
        height: 30px;
        border: 1px solid #cdcdcd;
        border-radius: 5px;
        margin: 10px 0;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thêm mới giáo viên</h1>
            </div>
        </div>
        <?php
        if (isset($error)) { ?>
            <p class="alert alert-danger"><?= $error ?></p>
        <?php


        }
        ?>
        <form method="post" enctype="multipart/form-data">
            <div class="row">

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="name">Họ và tên</label> <br>
                    <input type="text" name="name"> <br>
                    <label for="birthday">Ngày sinh</label> <br>
                    <input type="text" name="birthday"> <br>
                    <label for="sex">Giới tính</label> <br>
                    <select name="sex" style=" margin: 10px 0;height: 30px;width: 100px;">
                        <option value="1">Nam</option>
                        <option value="2">Nữ</option>
                    </select> <br>
                    <label for="phone">Số điện thoại</label> <br>
                    <input type="text" name="phone"> <br>
                    <label for="address">Địa chỉ</label> <br>
                    <input type="text" name="address"> <br>
                    <label for="edu">Tốt nghiệp đại học/Cao đẳng</label> <br>
                    <input type="text" name="edu"> <br>

                </div>

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label>Hình ảnh</label>
                    <input id="img" type="file" name="img" class="form-control hidden" onchange="changeImg(this)">
                    <img id="avatar" class="thumbnail" width="300px" height="200px" src="../public/img/new.png">

                    <label for="email">Email</label> <br>
                    <input type="text" name="email"> <br>
                    <label for="password">Password</label> <br>
                    <input type="text" name="password"> <br>
                </div>


            </div>
            <button type="submit" name="submit" class="btn btn-danger" style="margin-top: 10px;">Tạo mới</button>
            <a href="teacher_vp.php" class="btn btn-primary" style="margin-top: 10px;">Quay lại</a>
        </form>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>
<script>
    function changeImg(input) {
        //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            //Sự kiện file đã được load vào website
            reader.onload = function(e) {
                //Thay đổi đường dẫn ảnh
                $('#avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function() {
        $('#avatar').click(function() {
            $('#img').click();
        });
    });
</script>

<script>
    function quay_lai_trang_truoc() {
        history.back();
    }
</script>
<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>
<!-- <script src="../public/js/flot/jquery.flot.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.pie.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.resize.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.time.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.tooltip.min.js"></script> -->
<!-- <script src="../public/js/flot-data.js"></script> -->

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>


</body>

</html>