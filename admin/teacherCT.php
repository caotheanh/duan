<?php
include "header.php";
// include "../function.php";
?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Quản trị Giáo viên chính thức</h1>
            </div>
        </div>
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <a href="teacher_add.php" class="btn btn-primary">Thêm mới giáo viên </a>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Stt</th>
                            <th>Hình ảnh</th>
                            <th>Tên</th>
                            <th>Ngày sinh</th>
                            <th>Số điện thoại liên hệ</th>
                            <th>Quản trị</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $stt = 0;
                        foreach (getData("SELECT * FROM teacher") as $row) { ?>
                            <tr>
                                <td><?= $stt += 1 ?></td>
                                <td><img src="../public/img/teacher/<?=$row['img'] ?>" alt="" width="100px" height="100px"></td>
                                <td><?= $row['name'] ?></td>
                                <td><?= $row['birthday'] ?></td>
                                <td><?= $row['phone'] ?></td>
                                <td>
                                    <a href="teacher_details.php?id_teacher=<?=$row['id'] ?>" class="btn btn-primary">Xem chi tiết</a>
                                </td>
                            <?php

                            }
                            ?>

                    </tbody>
                </table>

            </div>

        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>
<script src="../public/js/startmin.js"></script>

</body>

</html>