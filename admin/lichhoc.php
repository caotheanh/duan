<?php require_once "header.php" ?>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Quản lý lịch học</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <table style="width:100%;">
            <thead>
            <th>Thứ 2</th>
            <th>Thứ 3</th>
            <th>Thứ 4</th>
            <th>Thứ 5</th>
            <th>Thứ 6</th>
            </thead>
            <tbody>
            <?php
            foreach (getData("SELECT * FROM menu") as $item) { ?>
                <tr>
                    <td>

                        <p><?= $item['Mon'] ?></p>
                        <a href="thucdon_update.php?month=2" class="btn btn-primary">Cập nhật thực đơn</a>
                    </td>
                    <td>

                        <p><?= $item['Tue'] ?></p>
                        <a href="thucdon_update.php?month=3" class="btn btn-primary">Cập nhật thực đơn</a>
                    </td>
                    <td>

                        <p><?= $item['Wed'] ?></p>
                        <a href="thucdon_update.php?month=4" class="btn btn-primary">Cập nhật thực đơn</a>
                    </td>
                    <td>

                        <p><?= $item['Thu'] ?></p>
                        <a href="thucdon_update.php?month=5" class="btn btn-primary">Cập nhật thực đơn</a>
                    </td>
                    <td>

                        <p><?= $item['Fri'] ?></p>
                        <a href="thucdon_update.php?month=6" class="btn btn-primary">Cập nhật thực đơn</a>
                    </td>
                </tr>
                <?php

            }
            ?>
            </tbody>
        </table>

        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>

</body>

</html>