<?php
include "header.php";
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    action("UPDATE tuvan SET status = 1 WHERE id = '$id'");
}
?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thông tin cần tư vấn</h1>
            </div>
        </div>
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Stt</th>
                            <th>Tên phụ huynh</th>
                            <th>Số điện thoại</th>
                            <th>Nội dung</th>
                            <th>Quản trị</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $stt = 0;
                        foreach (getData("SELECT * FROM tuvan") as $tv) { ?>
                            <tr>
                                <td><?= $stt += 1 ?></td>
                                <td><?= $tv['name'] ?></td>
                                <td><?= $tv['phone'] ?></td>
                                <td><?= $tv['content'] ?></td>

                                <td>
                                    <?php
                                        if ($tv['status'] == 0) { ?>
                                        <a href="tuvan.php?id=<?= $tv['id'] ?>" class="btn btn-primary">Tư vấn</a>
                                    <?php

                                        } else { ?>
                                        <a >Đã tư vấn</a>
                                    <?php

                                        }
                                        ?>
                                </td>
                            </tr>
                        <?php

                        }
                        ?>

                    </tbody>
                </table>

            </div>

        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>
<script src="../public/js/startmin.js"></script>

</body>

</html>