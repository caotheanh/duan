<?php
include "header.php";
$stt = 0;
if(isset($_GET['id'])){
    $id = $_GET['id'];
    action("DELETE FROM parents WHERE id = '$id'");
}
?>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Quản trị phụ huynh
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <a href="parent_add.php" class="btn btn-danger">Thêm mới phụ huynh</a>

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Stt</th>
                            <th>Tên bố</th>
                            <th>Tên mẹ</th>
                            <th>Số điện thoại bố</th>
                            <th>Số điện thoại mẹ</th>
                            <th>Quản trị</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        foreach (getData("SELECT * FROM parents") as $row) { ?>
                            <tr>
                                <td><?= $stt += 1 ?></td>
                                <td><?= $row['name_father'] ?></td>
                                <td><?= $row['name_mother'] ?></td>
                                <td><?= $row['phone_father'] ?></td>
                                <td><?= $row['phone_mother'] ?></td>
                                <td>
                                    <a href="parent_edit.php?id=<?= $row['id'] ?>" class="btn btn-primary">Sửa</a>
                                    <a href="parent.php?id=<?= $row['id'] ?>" class="btn btn-info">Xóa</a>
                                </td>
                            </tr>
                        <?php

                        }





                        ?>


                    </tbody>
                </table>

            </div>

        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>

</body>

</html>