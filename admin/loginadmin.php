<?php
session_start();
require_once("../function.php");
if(isset($_SESSION['admin']) || isset($_SESSION['teacher'])){
    header("Location:index.php");
};
if (isset($_POST['submit'])) {
    $phone = $_POST['phone'];
    $pass = md5($_POST['password']);
    $check = "SELECT * FROM admin WHERE phone = '$phone' AND password = '$pass'";
    $cout = $conn->prepare($check);
    $cout->execute();
    if ($cout->rowCount() > 0) {
        $_SESSION['admin'] = $phone;
        header("Location:index.php");
    } else {
        $error = "Đăng nhập thất bại";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ánh Mai Sáng</title>

    <link href="../public/css/bootstrap.min.css" rel="stylesheet">

    <link href="../public/css/metisMenu.min.css" rel="stylesheet">

    <link href="../public/css/startmin.css" rel="stylesheet">

    <link href="../public/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">

                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Đăng nhập admin</h3>
                    </div>
                    <?php
                    if (isset($error)) { ?>
                        <p class="alert alert-danger"><?= $error ?></p>
                    <?php

                    }
                    ?>
                    <form action="loginadmin.php" method="post">
                        <div class="panel-body">
                            <form role="form">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Số điện thoại" name="phone" type="number" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                    </div>
                                    <input type="submit" name="submit" value="Đăng nhập" class="btn btn-primary">
                                    <a href="login.php">Đăng nhập với tư cách giáo viên?</a>
                                </fieldset>
                            </form>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>