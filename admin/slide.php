<?php
include "header.php";
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    action("DELETE FROM slide WHERE id = '$id'");
}
?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Quản trị slide</h1>
            </div>
        </div>
        <div class="row">
            <a href="slide_add.php" class="btn btn-primary">Thêm mới slide</a>
           
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Stt</th>
                            <th>Hình ảnh</th>
                            <th>Trạng thái</th>
                            <th>Quản trị</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $stt = 0;
                        foreach (getData("SELECT * FROM slide") as $sl) { ?>
                            <tr>
                                <td><?= $stt += 1 ?></td>
                                <td><img src="../public/img/slide/<?= $sl['img'] ?>" alt="" width="150px" height="150px"></td>
                                <td><?= $sl['status']==1?"Hiện":"Ẩn" ?></td>
                                <td>
                                    <a href="slide_edit.php?id=<?=$sl['id'] ?>" class="btn btn-danger">Sửa</a>
                                    <a href="slide.php?id=<?=$sl['id'] ?>" onclick="return confirm()" class="btn btn-info">Xóa</a>
                                </td>
                            </tr>
                        <?php

                        }
                        ?>

                    </tbody>
                </table>

            </div>

        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>
<script src="../public/js/startmin.js"></script>

</body>

</html>