<?php
include "header.php";
if (isset($_SESSION['admin'])) {
    $phone = $_SESSION['admin'];
    foreach (getData("SELECT * FROM admin WHERE phone = '$phone'") as $admin) {
        $name = $admin['name'];
        $id_admin = $admin['id'];
    }
}
if (isset($_GET['id_post'])) {
    $id_post = $_GET['id_post'];
    foreach (getData("SELECT * FROM post WHERE id= '$id_post'") as $post) {
        $itile = isset($post['title']) ? $post['title'] : '';
        $content = isset($post['content']) ? $post['content'] : '';
        $id_cate = isset($post['id_cate']) ? $post['id_cate'] : '';
        // $id_admin = isset($post['id_admin']) ? $post['id_admin'] : '';
        $img = isset($post['img']) ? $post['img'] : '';
        $date_add = isset($post['date_add']) ? $post['date_add'] : '';
    }
}

if (isset($_POST['submit'])) {
    $date_new = Date('Y-m-d');
    $title_new = $_POST['title'];
    $content_new = $_POST['content'];
    $cate_id_new = $_POST['cate_id'];
    if (isset($_FILES['img']) && $_FILES['img']['name']) {
        $img_new = $_FILES['img'];
        $maxSize = 800000;
        $upload = true;
        $dir = "../public/img/";
        $target_file = $dir . basename($img_new['name']);
        $type = pathinfo($target_file, PATHINFO_EXTENSION);
        $allowtypes = array('jpg', 'png', 'jpeg');
        if ($img_new["size"] > $maxSize) {
            $error = "File ảnh quá lớn. Vui lòng chọn ảnh khác";
            $upload = false;
        } elseif (!in_array($type, $allowtypes)) {
            $error = "Chỉ được upload các định dạng JPG, PNG, JPEG";
            $upload = false;
        } else {
            $imgname = uniqid() . "-" . $img_new['name'];
            move_uploaded_file($img_new['tmp_name'], $dir . $imgname);
            action("UPDATE post SET title= '$title_new',content='$content_new',id_cate='$cate_id_new',id_admin='$id_admin',img= '$imgname' WHERE id = '$id_post' ");
            $error = "Cập nhật thành công";
        }
    } else {
        action("UPDATE post SET title= '$title_new',content='$content_new',id_cate='$cate_id_new',id_admin='$id_admin' WHERE id = '$id_post' ");
        $error = "Cập nhật thành công";
    }
}
?>
<style>
    form input {
        width: 100%;
        height: 30px;
        border: 1px solid #cdcdcd;
        border-radius: 5px;
        margin: 10px 0;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Cập nhật hoạt động</h1>
            </div>
        </div>
        <?php
        if (isset($error)) { ?>
            <p class="alert alert-danger"><?= $error ?></p>
        <?php

        }
        ?>
        <form method="post" enctype="multipart/form-data">
            <div class="row">

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="name">Tiêu đề</label> <br>
                    <input type="text" name="title" value="<?= $itile ?>"> <br>
                    <select name="cate_id" id="">

                        <?php
                        foreach (getData("SELECT * FROM category WHERE id= '$cate_id'") as $col) { ?>
                            <option value="<?= $cate_id ?>"><?= $col['name'] ?></option>
                        <?php
                        }
                        foreach (getData("SELECT * FROM category") as $cate) { ?>
                            <option value="<?= $cate['id'] ?>"><?= $cate['name'] ?></option>
                        <?php

                        }

                        ?>
                    </select> <br><br>

                    <label>Hình ảnh</label>
                    <input id="img" type="file" name="img" class="form-control hidden" onchange="changeImg(this)">
                    <img id="avatar" class="thumbnail" width="300px" height="200px" src="../public/img/<?= $img ?>">


                </div>

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="content">Nội dung</label>
                    <textarea name="content" id="" cols="30" rows="10" class="form-control"><?= $content ?></textarea>


                </div>


            </div>
            <button type="submit" name="submit" class="btn btn-danger" style="margin-top: 10px;">Cập nhật</button>
            <a href="post.php" class="btn btn-primary" style="margin-top: 10px;">Quay lại</a>
        </form>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>
<script>
    function changeImg(input) {
        //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            //Sự kiện file đã được load vào website
            reader.onload = function(e) {
                //Thay đổi đường dẫn ảnh
                $('#avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function() {
        $('#avatar').click(function() {
            $('#img').click();
        });
    });
</script>

<script>
    function quay_lai_trang_truoc() {
        history.back();
    }
</script>
<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>
<!-- <script src="../public/js/flot/jquery.flot.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.pie.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.resize.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.time.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.tooltip.min.js"></script> -->
<!-- <script src="../public/js/flot-data.js"></script> -->

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>


</body>

</html>