<?php
include "header.php";
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    action("DELETE FROM category WHERE id= '$id'");
}
if (isset($_POST['name'])) {
    $name = $_POST['name'];
    $sl = total("SELECT COUNT(*) FROM category WHERE name='$name'");
    if ($sl > 0) {
        $err = "Danh mục đã tồn tại";
    } else {
        action("INSERT INTO category (name) VALUES('$name')");
        header("Location:cate.php");
    }
}
?>
<div id="page-wrapper">
    <div class="container">
        <h1 class="text-center">Quản trị danh mục bài viết</h1>
        <div class="row" style="margin-top: 50px;">
            <form method="POST">

                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <?php
                    if (isset($err)) { ?>
                        <p class="alert alert-danger"><?= $err ?></p>
                    <?php

                    }
                    ?>
                    <input type="text" name="name" class="form-control"> <br>
                    <button type="submit" name="submit" class="btn btn-danger mt-3">Thêm mới</button>
                </div>
            </form>
            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                <?php
                foreach (getData("SELECT * FROM category") as $cate) { ?>
                    <p>
                        <?= $cate['name'] ?>
                        <a href="edit_cate.php?id=<?= $cate['id'] ?>" class="btn btn-primary">Sửa</a>
                        <a href="cate.php?id=<?= $cate['id'] ?>" class="btn btn-danger">Xóa</a>
                    </p>
                <?php

                }

                ?>
            </div>


        </div>
    </div>
</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>
<script src="../public/js/startmin.js"></script>

</body>

</html>