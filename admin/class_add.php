<?php
include "header.php";
// include "../function.php";
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    if (isset($_POST['submit'])) {
        $name = $_POST['name'];
        $sql = "SELECT * FROM class WHERE name = '$name' AND id_gr= '$id'";
        $cout = $conn->prepare($sql);
        $cout->execute();
        if ($cout->rowCount() > 0) {
            $error = "Lớp học đã tồn tại";
        } elseif (empty($_POST['name'])) {
            $error = "Không được để trống";
        } else {
            action("INSERT INTO class (name,id_gr) VALUES ('$name','$id')");
            header("Location:class_add.php?id=".$id);
        }
    } elseif (isset($_GET['id_class'])) {
        $id_class = $_GET['id_class'];
        action("DELETE FROM class WHERE id='$id_class'");
    }
} else {
    header("Location:index.php");
}
?>


<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Quản trị lớp</h1>
            </div>

        </div>
        <div class="row">

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding-right: 100px;">
                <?php
                if (isset($error)) { ?>
                    <p class="alert alert-danger"><?= $error ?></p>
                <?php

                }
                ?>
                <form method="post">
                    <label for="">Tên lớp học</label> <br> <br>
                    <input type="text" name="name" style="width:100%;border-radius: 5px;border: 1px solid #cdcdcd;height: 30px;"> <br> <br>
                    <input type="submit" name="submit" value="Tạo mới" class="btn btn-primary">
                    <a href="index.php" class="btn btn-danger">Quay lại</a>
                </form>

            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Tên lớp học</th>
                            <th>Hành động</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach (getData("SELECT * FROM class WHERE id_gr='$id'") as $row) { ?>
                            <tr>
                                <td><a href="class_details.php?id=<?=$row['id'] ?>"><?= $row['name'] ?></a></td>
                                <td>
                                    <a href="class_edit.php?id=<?= $id?>&id_class=<?= $row['id'] ?>" class="btn btn-success">Sửa</a>
                                    <a href="class_add.php?id=<?= $id?>&id_class=<?= $row['id'] ?>" class="btn btn-info">Xóa</a>
                                </td>
                            </tr>
                        <?php

                        }
                        ?>
                    </tbody>
                </table>



            </div>


        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>

</body>

</html>