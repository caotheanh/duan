<?php
include "header.php";
if (isset($_GET['id_class'])  && isset($_GET['th'])) {
    $id_class = $_GET['id_class'];
    $th = $_GET['th'];
    if (isset($_POST['submit'])) {
        $morning = $_POST['morning'];
        $noon = $_POST['noon'];
        $afternoon = $_POST['afternoon'];
        if (empty($morning) || empty($noon) || empty($afternoon)) {
            $error = "Vui lòng điền đầy đủ";
        }
        switch ($th) {
            case 2:
                action("INSERT INTO menu_mon (morning,noon,afternoon,id_class) VALUES ('$morning','$noon','$afternoon','$id_class')");
                $error = "Tạo mới thành công";
                break;
            case 3:
                action("INSERT INTO menu_tue (morning,noon,afternoon,id_class) VALUES ('$morning','$noon','$afternoon','$id_class')");
                $error = "Tạo mới thành công";
                break;
            case 4:
                action("INSERT INTO menu_wed (morning,noon,afternoon,id_class) VALUES ('$morning','$noon','$afternoon','$id_class')");
                $error = "Tạo mới thành công";
                break;
            case 5:
                action("INSERT INTO menu_thu (morning,noon,afternoon,id_class) VALUES ('$morning','$noon','$afternoon','$id_class')");
                $error = "Tạo mới thành công";
                break;
            case 6:
                action("INSERT INTO menu_fri (morning,noon,afternoon,id_class) VALUES ('$morning','$noon','$afternoon','$id_class')");
                $error = "Tạo mới thành công";
                break;
            default:
                header("Location:index.php");
                break;
        }
    }
    ?>
        <style>
            form input {
                width: 100%;
                height: 30px;
                border: 1px solid #cdcdcd;
                border-radius: 5px;
                margin: 10px 0;
            }
        </style>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Cập nhật thực đơn</h1>
                    </div>
                </div>

                <form method="post">


                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <?php
                            if (isset($error)) { ?>
                            <p class="alert alert-danger"><?= $error ?></p>
                        <?php


                            }
                            ?>
                        <label for="name">Buổi sáng</label> <br>
                        <input type="text" name="morning"> <br>
                        <label for="name">Buổi trưa</label> <br>
                        <input type="text" name="noon"> <br>
                        <label for="name">Buổi chiều</label> <br>
                        <input type="text" name="afternoon"> <br>
                        <button type="submit" name="submit" class="btn btn-danger" style="margin-top: 10px;">Cập nhật</button>
                    </div>




                </form>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="../public/js/jquery.min.js"></script>


        <script>
            function quay_lai_trang_truoc() {
                history.back();
            }
        </script>
        <!-- Bootstrap Core JavaScript -->
        <script src="../public/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../public/js/metisMenu.min.js"></script>

        <!-- Flot Charts JavaScript -->
        <script src="../public/js/flot/excanvas.min.js"></script>
        <script src="../public/js/startmin.js"></script>


        </body>

        </html>
    <?php
    } else {
        header("Location:index.php");
    }
    ?>