<?php
include "header.php";
?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Quản trị thông tin giới thiệu</h1>
            </div>
        </div>
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Stt</th>
                            <th>Số điện thoại</th>
                            <th>Email</th>
                            <th>Địa chỉ</th>
                            <th>Quản trị</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $stt = 0;
                        foreach (getData("SELECT * FROM info") as $info) { ?>
                            <tr>
                                <td><?= $stt += 1 ?></td>

                                <td><?= $info['phone'] ?></td>

                                <td><?= $info['email'] ?></td>
                                <td><?= $info['address'] ?></td>
                                <td>

                                    <a href="info_edit.php?id=<?= $info['id'] ?>" class="btn btn-primary">Sửa</a>

                                </td>
                            </tr>
                        <?php

                        }
                        ?>

                    </tbody>
                </table>

            </div>

        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>
<script src="../public/js/startmin.js"></script>

</body>

</html>