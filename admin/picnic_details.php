<?php
include "header.php";
if (isset($_GET['id_picnic'])) {
    $id = $_GET['id_picnic'];
}
?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Chi tiết hoạt động</h1>
            </div>
        </div>
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Stt</th>
                            <th>Tên học sinh</th>
                            <th>Lớp</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $stt = 0;
                        foreach (getData("SELECT * FROM details_picnic WHERE id_picnic = '$id'") as $row) {
                            $id_picnic = $row['id_picnic'];
                            $id_hs = $row['id_hs'];
                            foreach (getData("SELECT * FROM student WHERE id = '$id_hs'") as $hs) {
                                $id_class = $hs['id_class'];
                                foreach (getData("SELECT * FROM class WHERE id = '$id_class'") as $class) { ?>
                                    <tr>
                                        <td><?= $stt += 1 ?></td>
                                        <td><?= $hs['name'] ?></td>
                                        <td><?= $class['name'] ?></td>
                                    </tr>
                        <?php
                                }
                            }
                        }

                        ?>

                    </tbody>
                </table>
                <?php
                foreach (getData("SELECT * FROM picnic WHERE id = '$id'") as $row) { ?>
                    <p>Danh sách học sinh tham gia ngoại khóa <b> <?= $row['name'] ?></b></p>
                <?php

                }
                ?>
            </div>

        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>
<script src="../public/js/startmin.js"></script>

</body>

</html>