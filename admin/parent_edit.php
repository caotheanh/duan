<?php
include "header.php";
// include "../function.php";
if (isset($_GET['id'])) {
    $parents = $_GET['id'];
    foreach (getData("SELECT * FROM parents WHERE id = '$parents'") as $row) {
        $name_father = isset($row['name_father']) ? $row['name_father'] : '';
        $name_mother = isset($row['name_mother']) ? $row['name_mother'] : '';
        $phone_father = isset($row['phone_father']) ? $row['phone_father'] : '';
        $phone_mother = isset($row['phone_mother']) ? $row['phone_mother'] : '';
        $address_father = isset($row['address_father']) ? $row['address_father'] : '';
        $address_mother = isset($row['address_mother']) ? $row['address_mother'] : '';
        $birthday_father = isset($row['birthday_father']) ? $row['birthday_father'] : '';

        $birthday_mother = isset($row['birthday_mother']) ? $row['birthday_mother'] : '';
        $email = isset($row['email']) ? $row['email'] : '';
        $password = isset($row['password']) ? $row['password'] : '';
    }
    if (isset($_POST['submit'])) {
        $name_father_new = $_POST['name_father'];
        $name_mother_new = $_POST['name_mother'];
        $phone_father_new = $_POST['phone_father'];
        $phone_mother_new = $_POST['phone_mother'];
        $address_father_new = $_POST['address_father'];
        $address_mother_new = $_POST['address_mother'];
        $birthday_father_new = $_POST['birthday_father'];
        $birthday_mother_new = $_POST['birthday_mother'];
        $email_new = $_POST['email'];
        $password_new = $_POST['password'];
        action("UPDATE parents SET name_father='$name_father_new',name_mother='$name_mother_new',phone_father='$phone_father_new',
        phone_mother='$phone_mother_new',
        address_father='$address_father_new',address_mother='$address_mother_new',birthday_father='$birthday_father_new',
        birthday_mother='$birthday_mother_new',
        email='$email_new',
        password='$password_new' WHERE id = '$parents' ");
        $error = "Cập nhật thành công";
    }
}
?>
<style>
    form input {
        width: 100%;
        height: 30px;
        border: 1px solid #cdcdcd;
        border-radius: 5px;
        margin: 10px 0;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Cập nhật thông tin giáo viên</h1>
            </div>
        </div>
        <?php
        if (isset($error)) { ?>
            <p class="alert alert-danger"><?= $error ?></p>
        <?php


        }
        ?>
        <form method="post">
            <div class="row">

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="name_father">Họ tên bố</label> <br>
                    <input type="text" name="name_father" value="<?=$name_father ?>"> <br>
                    <label for="phone_father">Số điện thoại bố</label> <br>
                    <input type="text" name="phone_father" value="<?=$phone_father ?>"> <br>
                    <label for="name_mother">Họ tên mẹ</label> <br>
                    <input type="text" name="name_mother" value="<?=$name_mother ?>"> <br>
                    <label for="phone_mother">Số điện thoại mẹ</label> <br>
                    <input type="text" name="phone_mother" value="<?=$phone_mother ?>"> <br>
                    <label for="address_father">Địa chỉ bố</label> <br>
                    <input type="text" name="address_father" value="<?=$address_father ?>"> <br>



                </div>

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="address_mother">Địa chỉ mẹ</label> <br>
                    <input type="text" name="address_mother" value="<?=$address_mother ?>"> <br>
                    <label for="email">Email Phụ huynh</label> <br>
                    <input type="text" name="email" value="<?=$email ?>"> <br>
                    <label for="password">Password</label> <br>
                    <input type="text" name="password" value="<?=$password ?>"> <br>
                    <label for="birthday_father">Ngày sinh bố</label> <br>
                    <input type="text" name="birthday_father" value="<?=$birthday_father ?>"> <br>
                    <label for="birthday_mother">Ngày sinh mẹ</label> <br>
                    <input type="text" name="birthday_mother" value="<?=$birthday_mother ?>"> <br>
                </div>


            </div>
            <button type="submit" name="submit" class="btn btn-danger" style="margin-top: 10px;">Cập nhật</button>
            <a href="parent.php" class="btn btn-primary" style="margin-top: 10px;">Quay lại</a>
        </form>
    </div>

</div>
</div>
<script src="../public/js/jquery.min.js"></script>
<script>
    function changeImg(input) {
        //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            //Sự kiện file đã được load vào website
            reader.onload = function(e) {
                //Thay đổi đường dẫn ảnh
                $('#avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function() {
        $('#avatar').click(function() {
            $('#img').click();
        });
    });
</script>

<script>
    function quay_lai_trang_truoc() {
        history.back();
    }
</script>
<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>

<script src="../public/js/startmin.js"></script>


</body>

</html>