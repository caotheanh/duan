<?php
include "header.php";
if (isset($_GET['id'])) {
    $id_class = $_GET['id'];
    if (isset($_POST['submit'])) {
        $name = $_POST['name'];
        $birthday = $_POST['birthday'];
        $sex = $_POST['sex'];
        $status =1;
        $address = $_POST['address'];
        $id_ph = $_POST['id_ph'];
        $img = $_FILES['img'];
        $date = Date('Y-m-d');
        $maxSize = 800000;
        $upload = true;
        $dir = "../public/img/student/";
        $target_file = $dir . basename($img['name']);
        $type = pathinfo($target_file, PATHINFO_EXTENSION);
        $allowtypes = array('jpg', 'png', 'jpeg');
       
        if (
            empty($name) || empty($birthday) || empty($sex) || empty($address) ||
             empty($id_class) || empty($img) || empty($id_ph)
        ) {
            $error = "Vui lòng điền đầy đủ thông tin";
        }  elseif ($img["size"] > $maxSize) {
            $error = "File ảnh quá lớn. Vui lòng chọn ảnh khác";
            $upload = false;
        } elseif (!in_array($type, $allowtypes)) {
            $error = "Chỉ được upload các định dạng JPG, PNG, JPEG";
            $upload = false;
        } else {
            $imgname = uniqid() . "-" . $img['name'];
            if (move_uploaded_file($img['tmp_name'], $dir . $imgname)) { }

            $sql = "INSERT INTO student (name,birthday,sex,address,id_class,img,id_ph,date_start_study,status) VALUES 
            ('$name','$birthday','$sex','$address','$id_class','$imgname','$id_ph','$date','$status')";
            action($sql);
            $error = "Thêm mới thành công";
        }
    }
}

?>
<style>
    form input {
        width: 100%;
        height: 30px;
        border: 1px solid #cdcdcd;
        border-radius: 5px;
        margin: 10px 0;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thêm mới học sinh</h1>
            </div>
        </div>
        <?php
        if (isset($error)) { ?>
            <p class="alert alert-danger"><?= $error ?></p>
        <?php


        }

        ?>
        <form method="post" enctype="multipart/form-data">
            <div class="row">

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="name">Họ và tên</label> <br>
                    <input type="text" name="name"> <br>
                    <label for="birthday">Ngày sinh</label> <br>
                    <input type="text" name="birthday"> <br>
                    <label for="sex">Giới tính</label> <br>
                    <select name="sex" style=" margin: 10px 0;height: 30px;width: 100px;">
                        <option value="1">Nam</option>
                        <option value="2">Nữ</option>
                    </select> <br>
                    
                    <label for="address">Địa chỉ</label> <br>
                    <input type="text" name="address"> <br>
                    <label for="id_class">Lớp</label> <br>
                    <select name="id_class" style=" margin: 10px 0;height: 30px;width: 100px;">
                        <?php
                        foreach (getData("SELECT * FROM class WHERE id = '$id_class' ") as $row) { ?>
                            <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                        <?php

                        }
                        ?>
                    </select>

                </div>

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label>Hình ảnh</label>
                    <input id="img" type="file" name="img" class="form-control hidden" onchange="changeImg(this)">
                    <img id="avatar" class="thumbnail" width="300px" height="200px" src="../public/img/new.png">

                    <label for="id_ph">Email Phụ huynh</label> <br>
                    <select name="id_ph" style=" margin: 10px 0;height: 30px;width: 100px;">
                        <?php
                        foreach (getData("SELECT * FROM parents") as $parent) { ?>
                            <option value="<?= $parent['id'] ?>"><?= $parent['email'] ?></option>
                        <?php
                        }
                        ?>
                    </select> <br>
                   
                </div>


            </div>
            <button type="submit" name="submit" class="btn btn-danger" style="margin-top: 10px;">Tạo mới</button>
            <a href="student.php?id=<?= $id_class ?>" class="btn btn-primary" style="margin-top: 10px;">Quay lại</a>
        </form>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>
<script>
    function changeImg(input) {
        //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            //Sự kiện file đã được load vào website
            reader.onload = function(e) {
                //Thay đổi đường dẫn ảnh
                $('#avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function() {
        $('#avatar').click(function() {
            $('#img').click();
        });
    });
</script>

<script>
    function quay_lai_trang_truoc() {
        history.back();
    }
</script>
<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>
<!-- <script src="../public/js/flot/jquery.flot.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.pie.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.resize.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.time.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.tooltip.min.js"></script> -->
<!-- <script src="../public/js/flot-data.js"></script> -->

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>


</body>

</html>