<?php require_once "header.php";
$id_student = '';
if (isset($_SESSION['parent'])) {
    $phone = $_SESSION['parent'];
    foreach (getData("SELECT * FROM parents WHERE phone_mother = '$phone' OR phone_father= '$phone'") as $row) {
        $id = $row['id'];
    };
    foreach (getData("SELECT * FROM student WHERE id_ph='$id'") as $student) {
        $id_student = $student['id'];
    }
}
if (isset($_POST['submit'])) {
    $id_student = $_POST['id_student'];
    if (isset($_GET['id_picnic'])) {
        $id_picnic_register = $_GET['id_picnic'];
        foreach (getData("SELECT * FROM picnic WHERE id = '$id_picnic_register'") as $picnics) {
            $start = $picnics['start'];
            $stop = $picnics['stop'];
            $name = $picnics['name'];
        };
        // $id_student_register = $id_student;
        if (action("INSERT INTO details_picnic (id_picnic,id_hs,start,stop,name)
         VALUES('$id_picnic_register','$id_student','$start','$stop','$name')")) {
            echo "<script>alert('Đăng ký thành công')</script>";
        } else {
            echo "<script>alert('Học sinh đã đăng ký trước đó')</script>";
        }
    }
}
?>
<style>
    table tbody tr td {
        width: 20%;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Danh sách các hoạt động</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <?php
            foreach (getData("SELECT * FROM picnic") as $picnic) { ?>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

                    <div class="jumbotron">
                        <div class="container">
                            <img src="../public/img/picnic/<?= $picnic['img'] ?>" width="`00%" height="150px" alt="">
                            <h3><?= $picnic['name'] ?></h3>
                            <p style="font-size: 20px;"><?=substr($picnic['details'],0,150)  ?>...</p>
                            <p style="font-size: 13px;"><?= $picnic['start'] ?>&nbsp;- <?= $picnic['stop'] ?></p>
                            <a href="register_details_picnic.php?id_picnic=<?=$picnic['id'] ?>" class="btn btn-primary">
                                Đăng ký ngay
                            </a>

                            <!-- Modal -->
                            
                        </div>
                    </div>

                </div>

            <?php

            }
            ?>
        </div>

        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>

</body>

</html>