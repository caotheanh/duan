<?php require_once "header.php" ?>
<?php
if (isset($_SESSION['parent'])) {
    $phone = $_SESSION['parent'];
    foreach (getData("SELECT * FROM parents WHERE phone_father = '$phone' OR phone_mother= '$phone'") as $parent) {
        $id = $parent['id'];
        foreach (getData("SELECT * FROM student WHERE id_ph='$id'") as $student) {
            $id_class = $student['id_class'];
        }
    }
}

?>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center" style="margin-top: 20px;">
                <h3 class="text-themecolor m-b-0 m-t-0">Thông tin học sinh</h3>
            </div>

        </div>
        <div class="row">
            <!-- Column -->
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <?php
                    if (isset($id_class)) {
                        foreach (getData("SELECT * FROM teacher WHERE id_class='$id_class'") as $teacher) { ?>
                            <div class="card-block">
                                <div class="m-t-30" style="text-align: center;border-top:1px solid #cdcdcd ;"><img
                                            src="../public/img/teacher/<?= $teacher['img'] ?>" class="img-circle"
                                            width="150"/>
                                    <h4 class="card-title m-t-10"><?= $teacher['name'] ?></h4>

                                </div>
                            </div>
                            <div>
                                <hr>
                            </div>
                            <div class="card-block">
                                <div class="row">

                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <small class="text-muted p-t-30 db">Ngày sinh</small>
                                        <h6><?= $teacher['birthday'] ?></h6>
                                        <small class="text-muted p-t-30 db">Giới tính</small>
                                        <h6> <?= $teacher['sex'] == 1 ? 'Nam' : 'Nữ' ?></h6>
                                        <small class="text-muted p-t-30 db">Địa chỉ</small>
                                        <h6><?= $teacher['address'] ?></h6>


                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">


                                        <small class="text-muted p-t-30 db">Tốt nghiệp đại học</small>
                                        <h6><?= $teacher['education'] ?></h6>
                                        <small class="text-muted p-t-30 db">Email</small>
                                        <h6><?= $teacher['email'] ?></h6>
                                        <small class="text-muted p-t-30 db">Số điện thoại</small>
                                        <h6> <?= $teacher['phone'] ?></h6>
                                    </div>

                                </div>
                            </div>
                            <?php

                        }
                    }

                    ?>

                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">

                    <!-- Tab panes -->


                </div>
            </div>
            <!-- Column -->
        </div>

    </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>

</body>

</html>