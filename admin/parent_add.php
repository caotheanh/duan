<?php
include "header.php";
    if (isset($_POST['submit'])) {
        $name_father = $_POST['name_father'];
        $name_mother = $_POST['name_mother'];
        $phone_father = $_POST['phone_father'];
        $phone_mother = $_POST['phone_mother'];
        $address_father = $_POST['address_father'];
        $address_mother = $_POST['address_mother'];
        $birthday_father = $_POST['birthday_father'];
        $birthday_mother = $_POST['birthday_mother'];
        $email = $_POST['email'];
        $password = $_POST['password'];


        $check_phone_father = "SELECT * FROM parents WHERE phone_father= '$phone_father'";
        $check_phone_mother = "SELECT * FROM parents WHERE phone_mother= '$phone_mother'";
        $check_email = "SELECT * FROM parents WHERE email= '$email'";
        $cout_email = $conn->prepare($check_email);
        $cout_email->execute();
        $cout_phone_father = $conn->prepare($check_phone_father);
        $cout_phone_father->execute();
        $cout_phone_mother = $conn->prepare($check_phone_mother);
        $cout_phone_mother->execute();

        if (
            empty($name_father) || empty($name_mother) || empty($phone_father) || empty($phone_mother) ||
            empty($address_father) || empty($address_mother) || empty($birthday_father) ||
            empty($birthday_mother) || empty($email) || empty($password)
        ) {
            $error = "Vui lòng điền đầy đủ thông tin";
        } elseif ($cout_email->rowCount() > 0) {
            $error = "Email này đã có người sử dụng. Vui lòng chọn Email khác! ";
        } elseif ($cout_phone_father->rowCount() > 0) {
            $error = "Số điện thoại bố đã có người sử dụng. Vui lòng chọn Email khác! ";
        } elseif ($cout_phone_mother->rowCount() > 0) {
            $error = "Số điện thoại mẹ đã có người sử dụng. Vui lòng chọn Email khác! ";
        } else {


            $sql = "INSERT INTO parents (name_father,name_mother,phone_father,phone_mother,address_father,address_mother,birthday_father,birthday_mother,email,password) VALUES 
            ('$name_father','$name_mother','$phone_father','$phone_mother','$address_father','$address_mother','$birthday_father','$birthday_mother','$email','$password')";
            action($sql);
            $error = "Thêm mới thành công";
        }
    }

?>
<style>
    form input {
        width: 100%;
        height: 30px;
        border: 1px solid #cdcdcd;
        border-radius: 5px;
        margin: 10px 0;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thêm mới phụ huynh</h1>
            </div>
        </div>
        <?php
        if (isset($error)) { ?>
            <p class="alert alert-danger"><?= $error ?></p>
        <?php
        }
        ?>
        <form method="post">
            <div class="row">

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="name_father">Họ tên bố</label> <br>
                    <input type="text" name="name_father"> <br>
                    <label for="phone_father">Số điện thoại bố</label> <br>
                    <input type="text" name="phone_father"> <br>
                    <label for="name_mother">Họ tên mẹ</label> <br>
                    <input type="text" name="name_mother"> <br>
                    <label for="phone_mother">Số điện thoại mẹ</label> <br>
                    <input type="text" name="phone_mother"> <br>
                    <label for="address_father">Địa chỉ bố</label> <br>
                    <input type="text" name="address_father"> <br>



                </div>

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="address_mother">Địa chỉ mẹ</label> <br>
                    <input type="text" name="address_mother"> <br>
                    <label for="email">Email Phụ huynh</label> <br>
                    <input type="text" name="email"> <br>
                    <label for="password">Password</label> <br>
                    <input type="text" name="password"> <br>
                    <label for="birthday_father">Ngày sinh bố</label> <br>
                    <input type="text" name="birthday_father"> <br>
                    <label for="birthday_mother">Ngày sinh mẹ</label> <br>
                    <input type="text" name="birthday_mother"> <br>
                </div>


            </div>
            <button type="submit" name="submit" class="btn btn-danger" style="margin-top: 10px;">Tạo mới</button>
            <a href="parent.php" class="btn btn-primary" style="margin-top: 10px;">Quay lại</a>
        </form>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>


<script>
    function quay_lai_trang_truoc() {
        history.back();
    }
</script>
<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>
<!-- <script src="../public/js/flot/jquery.flot.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.pie.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.resize.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.time.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.tooltip.min.js"></script> -->
<!-- <script src="../public/js/flot-data.js"></script> -->

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>


</body>

</html>