<?php
include "header.php";
if (isset($_GET['id_picnic'])) {
    $id = $_GET['id_picnic'];
    action("DELETE FROM picnic WHERE id = '$id'");
}
?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Quản trị hoạt động ngoại khóa</h1>
            </div>
        </div>
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <a href="picnic_add.php" class="btn btn-primary">Thêm mới</a>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Stt</th>
                            <th>Hình ảnh</th>
                            <th>Tên hoạt động</th>
                            
                            <th>Ngày Bắt đầu</th>
                            <th>Ngày kết thúc</th>
                            <th>Quản trị</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $stt = 0;
                        foreach (getData("SELECT * FROM picnic") as $picnic) { ?>
                            <tr>
                                <td><?= $stt += 1 ?></td>
                                <td><img width="150px" height="150px" src="../public/img/picnic/<?= $picnic['img'] ?>" alt=""></td>
                                <td><?= $picnic['name'] ?></td>
                               
                                <td><?= $picnic['start'] ?></td>
                                <td><?= $picnic['stop'] ?></td>
                                <td>
                                    <a href="picnic_details.php?id_picnic=<?=$picnic['id'] ?>">Danh sách hs</a>
                                    <a href="picnic_edit.php?id_picnic=<?= $picnic['id'] ?>" class="btn btn-primary">Sửa</a>
                                    <a href="picnic.php?id_picnic=<?= $picnic['id'] ?>" class="btn btn-danger" onclick="return confirm('Delete?')">Xóa</a>
                                </td>
                            </tr>
                        <?php

                        }
                        ?>

                    </tbody>
                </table>

            </div>

        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>
<script src="../public/js/startmin.js"></script>

</body>

</html>