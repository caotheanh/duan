<?php
include "header.php";
// include "../function.php";
if (isset($_GET['id_teacher'])) {
    $id_teacher = $_GET['id_teacher'];
    foreach (getData("SELECT * FROM teacher_vp WHERE id = '$id_teacher'") as $row) {
        $name = isset($row['name']) ? $row['name'] : '';
        $birthday = isset($row['birthday']) ? $row['birthday'] : '';
        $sex = isset($row['sex']) ? $row['sex'] : '';
        // $id_class = isset($row['id_class']) ? $row['id_class'] : '';
        $phone = isset($row['phone']) ? $row['phone'] : '';
        $education = isset($row['education']) ? $row['education'] : '';
        $img = isset($row['img']) ? $row['img'] : '';

        $address = isset($row['address']) ? $row['address'] : '';
        $email = isset($row['email']) ? $row['email'] : '';
        $start_work = isset($row['start_work']) ? $row['start_work'] : '';
    }
    if (isset($_POST['submit'])) {
        $name_new = $_POST['name'];
        $birthday_new = $_POST['birthday'];
        $sex_new = $_POST['sex'];
        // $id_class_new = $_POST['id_class'];
        $phone_new = $_POST['phone'];
        $edu_new = $_POST['education'];
        $email_new = $_POST['email'];
        $address_new = $_POST['address'];
        $start_work = Date('Y-m-d');
        if (isset($_FILES['img']) && $_FILES['img']['name']) {
            $img_new = $_FILES['img'];
            $maxSize = 800000;
            $upload = true;
            $dir = "../public/img/teacher/";
            $target_file = $dir . basename($img_new['name']);
            $type = pathinfo($target_file, PATHINFO_EXTENSION);
            $allowtypes = array('jpg', 'png', 'jpeg');
            if ($img_new["size"] > $maxSize) {
                $error = "File ảnh quá lớn. Vui lòng chọn ảnh khác";
                $upload = false;
            } elseif (!in_array($type, $allowtypes)) {
                $error = "Chỉ được upload các định dạng JPG, PNG, JPEG";
                $upload = false;
            } else {
                $imgname = uniqid() . "-" . $img_new['name'];
                move_uploaded_file($img_new['tmp_name'], $dir . $imgname);
                action("UPDATE teacher_vp SET name = '$name_new',birthday = '$birthday_new',sex = '$sex_new',
                phone = '$phone_new',education = '$edu_new',
                img = '$imgname',address = '$address_new',
                start_work = '$start_work',email='$email_new'");
                header("Location:teacher_details_vp.php?id_teacher=$id_teacher");
            }
        } else {
            action("UPDATE teacher_vp SET name = '$name_new',birthday = '$birthday_new',sex = '$sex_new',
                phone = '$phone_new',education = '$edu_new',
                address = '$address_new',
                start_work = '$start_work',email='$email_new'");
            header("Location:teacher_details_vp.php?id_teacher=$id_teacher");
        }
    }
}
?>
<style>
    form input {
        width: 100%;
        height: 30px;
        border: 1px solid #cdcdcd;
        border-radius: 5px;
        margin: 10px 0;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Cập nhật thông tin giáo viên</h1>
            </div>
        </div>
        <?php
        if (isset($error)) { ?>
            <p class="alert alert-danger"><?= $error ?></p>
        <?php


        }
        ?>
        <form method="post" enctype="multipart/form-data">
            <div class="row">

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="name">Họ và tên</label> <br>
                    <input type="text" name="name" value="<?= $name ?>"> <br>
                    <label for="birthday">Ngày sinh</label> <br>
                    <input type="text" name="birthday" value="<?= $birthday ?>"> <br>
                    <label for="sex">Giới tính</label> <br>
                    <select name="sex" style=" margin: 10px 0;height: 30px;width: 100px;">
                        <?php
                        if ($sex == 1) { ?>
                            <option selected value="1">Nam</option>
                            <option value="2">Nữ</option>
                        <?php
                        } else { ?>
                            <option selected value="2">Nữ</option>
                            <option value="1">Nam</option>
                        <?php

                        }
                        ?>


                    </select> <br>
                    <label for="phone">Số điện thoại</label> <br>
                    <input type="text" name="phone" value="<?= $phone ?>"> <br>
                    <label for="address">Địa chỉ</label> <br>
                    <input type="text" name="address" value="<?= $address ?>"> <br>
                    <label for="edu">Tốt nghiệp đại học/Cao đẳng</label> <br>
                    <input type="text" name="education" value="<?= $education ?>"> <br>

                </div>

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label>Hình ảnh</label>
                    <input id="img" type="file" name="img" class="form-control hidden" onchange="changeImg(this)">
                    <img id="avatar" class="thumbnail" width="300px" height="200px" src="../public/img/teacher/<?= $img ?>">

                    <label for="email">Email</label> <br>
                    <input type="text" name="email" value="<?= $email ?>"> <br>

                    <label for="id_class">Quản lý lớp</label> <br>
                    <select name="id_class" style="height: 30px;width: 100px;">
                        <?php
                        foreach (getData("SELECT * FROM class") as $class) {
                            foreach (getData("SELECT * FROM class WHERE id = '$id_class'") as $item) { ?>
                                <option value="<?= $item['id'] ?>"><?= $item['name'] ?></option>
                            <?php

                                } ?>
                            <option value="<?= $class['id'] ?>"><?= $class['name'] ?></option>
                        <?php
                        }
                        ?>
                    </select> <br>


                </div>


            </div>
            <button type="submit" name="submit" class="btn btn-danger" style="margin-top: 10px;">Cập nhật</button>
            <a href="teacher_repassword_vp.php?id_teacher=<?= $id_teacher ?>" class="btn btn-primary" style="margin-top: 10px;">Đổi mật khẩu</a>
            <a href="teacher_vp.php" class="btn btn-primary" style="margin-top: 10px;">Quay lại</a>
        </form>
    </div>

</div>
</div>
<script src="../public/js/jquery.min.js"></script>
<script>
    function changeImg(input) {
        //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            //Sự kiện file đã được load vào website
            reader.onload = function(e) {
                //Thay đổi đường dẫn ảnh
                $('#avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function() {
        $('#avatar').click(function() {
            $('#img').click();
        });
    });
</script>

<script>
    function quay_lai_trang_truoc() {
        history.back();
    }
</script>
<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>

<script src="../public/js/startmin.js"></script>


</body>

</html>