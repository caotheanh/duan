<?php require_once "header.php";
$id_student = '';
if (isset($_SESSION['parent'])) {
    $phone = $_SESSION['parent'];
    foreach (getData("SELECT * FROM parents WHERE phone_mother = '$phone' OR phone_father= '$phone'") as $row) {
        $id = $row['id'];
    };
}
?>
<style>
    table tbody tr td {
        width: 20%;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Danh sách các hoạt động đã đăng ký</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <?php
            foreach (getData("SELECT * FROM student WHERE id_ph='$id'") as $student) {
                $id_student = $student['id'];
                foreach (getData("SELECT * FROM details_picnic WHERE id_hs='$id_student'") as $picnic) { ?>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

                        <div class="jumbotron">
                            <div class="container">
                                <h3><?= $picnic['name'] ?></h3>
                                <p><?= $picnic['start'] ?>&nbsp;- <?= $picnic['stop'] ?></p>
                                <?=$student['name'] ?>
                            </div>
                        </div>

                    </div>

            <?php

                }
            }

            ?>
        </div>

        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>

</body>

</html>