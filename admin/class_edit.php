<?php
include "header.php";
// include "../function.php";
if (isset($_GET['id']) && isset($_GET['id_class'])) {
    $id = $_GET['id'];
    $id_class = $_GET['id_class'];
    foreach (getData("SELECT * FROM class WHERE id_gr='$id' AND id='$id_class'") as $item) {
        $name = isset($item['name']) ? $item['name'] : '';
    }

    if (isset($_POST['submit'])) {

        $name_new = $_POST['name'];
        $sql = "SELECT * FROM class WHERE name = '$name_new' AND id_gr= '$id'";
        $count = $conn->prepare($sql);
        $count->execute();
        if ($count->rowCount() > 0) {
            $error = "Lớp học đã tồn tại";
        } elseif (empty($_POST['name'])) {
            $error = "Không được để trống";
        } else {
            action("UPDATE class SET name = '$name_new' WHERE id='$id_class'");
            header("Location:index.php");
            exit;
            $error = "Cập nhật thành công";
           
        }
    }
} else {
    header("Location:index.php");
}
?>


<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Quản trị lớp</h1>
            </div>

        </div>
        <div class="row">

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding-right: 100px;">
                <?php
                if (isset($error)) { ?>
                    <p class="alert alert-danger"><?= $error ?></p>
                <?php

                }
                ?>
                <form method="post" action="class_update.php?id=<?=$id?>&id_class=<?=$id_class?>">
                    <label for="">Tên lớp học</label> <br> <br>
                    <input type="text" name="name" style="width:100%;border-radius: 5px;border: 1px solid #cdcdcd;height: 30px;" value="<?= $name ?>"> <br> <br>
                    <input type="submit" name="submit" value="Cập nhật" class="btn btn-primary">
                    <a href="class_add.php?id=<?= $id ?>" class="btn btn-danger">Quay lại</a>
                </form>

            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>

</body>

</html>