<?php
ob_start();
session_start();
if (isset($_SESSION['admin']) || isset($_SESSION['teacher']) || isset($_SESSION['parent'])) {
    include("../function.php");
    ?>

    <!DOCTYPE html>

    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sổ liên lạc điện tử Ánh Mai Sáng</title>

        <!-- Bootstrap Core CSS -->
        <link href="../public/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../public/css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../public/css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../public/css/startmin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="../public/css/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../public/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php
                                                        if (isset($_SESSION['parent'])) {
                                                            echo "hello.php";
                                                        } elseif (isset($_SESSION['teacher'])) {
                                                            echo "hello.php";
                                                        }
                                                        ?>">Ánh Mai Sáng</a>
                </div>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><a href="#"><i class="fa fa-home fa-fw"></i>Sổ liên lạc điện tử</a></li>
                </ul>

                <ul class="nav navbar-right navbar-top-links">
                   
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>
                            <?php
                                if (isset($_SESSION['admin'])) {
                                    echo $_SESSION['admin'];
                                } elseif (isset($_SESSION['teacher'])) {
                                    echo $_SESSION['teacher'];
                                } elseif (isset($_SESSION['parent'])) {
                                    echo $_SESSION['parent'];
                                }
                                ?>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#"><i class="fa fa-user fa-fw"></i> Thông tin</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Đăng xuất</a>
                            </li>
                        </ul>
                    </li>
                </ul>


                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>

                            </li>
                            <?php
                                if (isset($_SESSION['admin'])) { ?>
                                <li>
                                    <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Quản lý lớp</a>
                                </li>
                                <li>
                                    <a href="#"><i class="glyphicon glyphicon-user"></i> Quản lý giáo viên<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="teacherCT.php">Giáo viên giảng dạy</a>
                                        </li>
                                        <li>
                                            <a href="teacher_vp.php">Giáo viên văn phòng</a>
                                        </li>
                                    </ul>

                                </li>

                                <li>
                                    <a href="#"><i class="glyphicon glyphicon-user"></i> Quản lý học sinh<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <?php
                                                foreach (getData("SELECT * FROM class") as $class) { ?>
                                            <li><a href="student.php?id=<?= $class['id'] ?>"><?= $class['name'] ?></a></li>
                                        <?php


                                                }
                                                ?>
                                    </ul>

                                </li>
                                <li>
                                    <a href="parent.php"><i class="glyphicon glyphicon-user"></i> Quản lý phụ huynh<span class="fa arrow"></span></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-edit fa-fw"></i> Quản lý thực đơn<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <?php
                                                foreach (getData("SELECT * FROM class") as $class) { ?>
                                            <li>
                                                <a href="thucdon.php?id_class=<?= $class['id'] ?>"><?= $class['name'] ?></a>
                                            </li>
                                        <?php
                                                }
                                                ?>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#"><i class="glyphicon glyphicon-user"></i>Sĩ số<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <?php
                                                foreach (getData("SELECT * FROM class") as $class) { ?>
                                            <li><a href="student_number.php?id=<?= $class['id'] ?>"><?= $class['name'] ?></a></li>
                                        <?php
                                                }
                                                ?>
                                    </ul>

                                </li>
                                <li>
                                    <a href="picnic.php"><i class="glyphicon glyphicon"></i>Quản trị hoạt động ngoại khóa<span class="fa arrow"></span></a>
                                </li>
                                <li>
                                    <a href="cate.php"><i class="glyphicon glyphicon"></i>Quản trị danh mục<span class="fa arrow"></span></a>
                                </li>
                                <li>
                                    <a href="post.php"><i class="glyphicon glyphicon"></i>Quản trị Bài viết<span class="fa arrow"></span></a>
                                </li>
                                <li>
                                    <a href="tuvan.php"><i class="glyphicon glyphicon"></i>Thông tin cần tư vấn<span class="fa arrow"></span></a>
                                </li>
                                <li>
                                    <a href="slide.php"><i class="glyphicon glyphicon"></i>Quản trị slide<span class="fa arrow"></span></a>
                                </li>
                                <li>
                                    <a href="info.php"><i class="glyphicon glyphicon"></i>Quản trị thông tin<span class="fa arrow"></span></a>
                                </li>
                            <?php

                                } elseif (isset($_SESSION['teacher'])) {
                                    $phone = $_SESSION['teacher'];
                                    foreach (getData("SELECT * FROM teacher WHERE phone='$phone'") as $teacher) {
                                        $id = $teacher['id_class'];
                                    }
                                    ?>
                                <li>
                                    <a href="#"><i class="fa fa-sitemap fa-fw"></i> Điểm danh<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <?php
                                                foreach (getData("SELECT * FROM class WHERE id = '$id'") as $class) { ?>
                                            <li><a href="diemdanh.php?id=<?= $class['id'] ?>"><?= $class['name'] ?></a></li>
                                        <?php
                                                }
                                                ?>
                                    </ul>

                                </li>

                            <?php

                                } elseif (isset($_SESSION['parent'])) { ?>
                                <li>
                                    <a href="#"><i class="fa fa-files-o fa-fw"></i> Phụ huynh<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="profileS.php">Thông tin</a>
                                        </li>
                                        <li>
                                            <a href="profileT.php">Cô giáo chủ nhiệm</a>
                                        </li>
                                        <!-- <li>
                                            <a href="thucdonP.php">Thực đơn</a>
                                        </li> -->
                                        <li>
                                            <a href="register_picnic.php">Đăng ký ngoại khóa</a>
                                        </li>
                                        <li>
                                            <a href="picnic_ds.php">Danh sách ngoại khóa đã đăng ký</a>
                                        </li>
                                        <li>
                                            <a href="nghi.php">Số ngày nghỉ trong tháng</a>
                                        </li>
                                        <li>
                                            <a href="ndthongbao.php">Thông báo của nhà trường</a>
                                        </li>
                                        <li style="display:none">
                                            <a href="login.php">Login Page</a>
                                        </li>
                                    </ul>

                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-edit fa-fw"></i> Xem thực đơn<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <?php
                                                $phone = $_SESSION['parent'];
                                                foreach (getData("SELECT * FROM parents WHERE phone_mother = '$phone' OR phone_father = '$phone'") as $parent) {
                                                    $id_ph = $parent['id'];
                                                    foreach (getData("SELECT DISTINCT id_class FROM student WHERE id_ph = '$id_ph'") as $student) {
                                                        $id_class = $student['id_class'];
                                                        foreach (getData("SELECT * FROM class WHERE id = '$id_class'") as $class) {

                                                            ?>
                                                    <li>
                                                        <a href="thucdonP.php?id_class=<?= $class['id'] ?>"><?= $class['name'] ?></a>
                                                    </li>
                                        <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                    </ul>
                                </li>
                            <?php

                                }
                                ?>


                        </ul>
                    </div>
                </div>
            </nav>
        <?php

        } else {
            header("Location:login.php");
        }

        ?>
       