<?php

if (isset($_GET['id'])) {
    include "header.php";
    $id = $_GET['id'];
    $stt = 0;
    ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Quản trị số lượng học sinh ăn tại trường lớp
                        <?php foreach (getData("SELECT * FROM class WHERE id ='$id'") as $row) { ?>
                            <?= $row['name'] ?></h1>
                <?php

                    } ?>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <?php
                    $total = total("SELECT COUNT(*) FROM student WHERE status =1 AND id_class ='$id'");
                    $date = Date('Y-m-d');
                    foreach (getData("SELECT * FROM menu_admin WHERE id_class ='$id'") as $row) {
                        $sl = $total- $row['number_student'];
                    } ?>
                <?php
                    if (isset($sl)) { ?>
                    <h3>Số học sinh ăn tại trường ngày <?= $date ?>: <b><?= $sl ?></b> </h3>
                <?php
                    } else {
                        $sl = 0;
                        ?>
                    <h3>Số học sinh ăn tại trường ngày <?= $date ?>: <b><?= $sl ?></b> </h3>
                <?php
                    }

                    ?>

            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../public/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../public/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../public/js/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../public/js/startmin.js"></script>

    </body>

    </html>
<?php
} else {
    header("Location:index.php");
}
?>