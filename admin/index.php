<?php
require_once "header.php";
if (!isset($_SESSION['parent']) && !isset($_SESSION['teacher'])) { ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Sổ liên lạc điện tử trường mầm non Ánh Mai Sáng</h1>
                </div>
            </div>

            <div class="row">
                <?php

                foreach (getData("SELECT * FROM group_class") as $row) { ?>
                    <div class="col-lg-3 col-md-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3><?= $row['name'] ?></h3>
                            </div>

                            <div class="panel-footer">
                                <ul>
                                    <?php
                                    foreach (getData("SELECT * FROM class WHERE id_gr=" . $row['id']) as $item) { ?>
                                        <li><a href="class_details.php?id=<?= $item['id'] ?>"><?= $item['name'] ?></a>
                                        </li>

                                        <?php

                                    }
                                    ?>
                                </ul>
                                <p><a href="class_add.php?id=<?= $row['id'] ?>">Quản trị</a></p>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>
                    <?php

                }
                ?>


            </div>

            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../public/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../public/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../public/js/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../public/js/raphael.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../public/js/startmin.js"></script>

    </body>

    </html>
    <?php
}else{?>
    <script src="../public/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../public/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../public/js/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../public/js/raphael.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../public/js/startmin.js"></script>
<?php

}
?>
