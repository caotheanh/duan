<?php
include "header.php";
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    foreach (getData("SELECT * FROM slide WHERE id= '$id'") as $slide) {
        $itile = isset($slide['title']) ? $slide['title'] : '';
        $link = isset($slide['link']) ? $slide['link'] : '';
        $status = isset($slide['status']) ? $slide['status'] : '';
        $img = isset($slide['img']) ? $slide['img'] : '';
    }
}

if (isset($_POST['submit'])) {
    $title_new = $_POST['title'];
    $link_new = $_POST['link'];
    $status_new = $_POST['status'];
    if (isset($_FILES['img']) && $_FILES['img']['name']) {
        $img_new = $_FILES['img'];
        $maxSize = 800000;
        $upload = true;
        $dir = "../public/img/slide/";
        $target_file = $dir . basename($img_new['name']);
        $type = pathinfo($target_file, PATHINFO_EXTENSION);
        $allowtypes = array('jpg', 'png', 'jpeg');
        if ($img_new["size"] > $maxSize) {
            $error = "File ảnh quá lớn. Vui lòng chọn ảnh khác";
            $upload = false;
        } elseif (!in_array($type, $allowtypes)) {
            $error = "Chỉ được upload các định dạng JPG, PNG, JPEG";
            $upload = false;
        } else {
            $imgname = uniqid() . "-" . $img_new['name'];
            move_uploaded_file($img_new['tmp_name'], $dir . $imgname);
            action("UPDATE slide SET title= '$title_new',link='$link_new',status='$status_new',img= '$imgname' WHERE id = '$id' ");
            $error = "Cập nhật thành công";
        }
    } else {
        action("UPDATE slide SET title= '$title_new',link='$link_new',status='$status_new' WHERE id = '$id' ");
        $error = "Cập nhật thành công";
    }
}
?>
<style>
    form input {
        width: 100%;
        height: 30px;
        border: 1px solid #cdcdcd;
        border-radius: 5px;
        margin: 10px 0;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Cập nhật slide</h1>
            </div>
        </div>
        <?php
        if (isset($error)) { ?>
            <p class="alert alert-danger"><?= $error ?></p>
        <?php

        }
        ?>
        <form method="post" enctype="multipart/form-data">
            <div class="row">

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="title">Tiêu đề</label> <br>
                    <input type="text" name="title" value="<?= $itile ?>"> <br>
                    <label for="link">Đường dẫn</label> <br>
                    <input type="text" name="link" value="<?= $link ?>"> <br>
                    <select name="status" id="">
                        <option value="<?= $status ?>" selected><?= $status == 0 ? "Ẩn" : "Hiện" ?></option>
                        <option value="<?= $status == 1 ? 0 : '1' ?>"><?= $status == 1 ? "Ẩn" : 'Hiện' ?></option>
                    </select> <br><br>

                    <label>Hình ảnh</label>
                    <input id="img" type="file" name="img" class="form-control hidden" onchange="changeImg(this)">
                    <img id="avatar" class="thumbnail" width="300px" height="200px" src="../public/img/slide/<?= $img ?>">


                </div>
            </div>


    </div>
    <button type="submit" name="submit" class="btn btn-danger" style="margin-top: 10px;">Cập nhật</button>
    <a href="slide.php" class="btn btn-primary" style="margin-top: 10px;">Quay lại</a>
    </form>
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>
<script>
    function changeImg(input) {
        //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            //Sự kiện file đã được load vào website
            reader.onload = function(e) {
                //Thay đổi đường dẫn ảnh
                $('#avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function() {
        $('#avatar').click(function() {
            $('#img').click();
        });
    });
</script>

<script>
    function quay_lai_trang_truoc() {
        history.back();
    }
</script>
<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>
<!-- <script src="../public/js/flot/jquery.flot.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.pie.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.resize.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.time.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.tooltip.min.js"></script> -->
<!-- <script src="../public/js/flot-data.js"></script> -->

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>


</body>

</html>