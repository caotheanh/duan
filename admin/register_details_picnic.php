<?php require_once "header.php";
if (isset($_GET['id_picnic'])) {
    $id_picnic_register = $_GET['id_picnic'];
    if (isset($_SESSION['parent'])) {
        $phone = $_SESSION['parent'];
        foreach (getData("SELECT * FROM parents WHERE phone_mother = '$phone' OR phone_father= '$phone'") as $row) {
            $id = $row['id'];
        };
        foreach (getData("SELECT * FROM student WHERE id_ph='$id'") as $student) {
            $id_student = $student['id'];
        }
    }
    if (isset($_POST['submit'])) {
        $id_student = $_POST['id_student'];
        foreach (getData("SELECT * FROM picnic WHERE id = '$id_picnic_register'") as $picnics) {
            $start = $picnics['start'];
            $stop = $picnics['stop'];
            $name = $picnics['name'];
        };
        $sl = total("SELECT COUNT(*) FROM details_picnic WHERE id_picnic='$id_picnic_register' AND id_hs='$id_student'");
        if ($sl == 0) {
            action("INSERT INTO details_picnic (id_picnic,id_hs,start,stop,name)
            VALUES('$id_picnic_register','$id_student','$start','$stop','$name')");
            echo "<script>alert('Đăng ký thành công')</script>";
        } else {
            echo "<script>alert('Học sinh này đã đăng ký')</script>";
        }
    }
}
?>
<style>
    table tbody tr td {
        width: 20%;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Xác nhận đăng ký</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <p class="alert alert-danger">Vui lòng suy nghĩ kỹ trước khi đăng ký. Đăng ký xong không thể hủy?</p>
            <?php
            foreach (getData("SELECT * FROM picnic WHERE id= '$id_picnic_register'") as $picnic) { ?>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">

                    <div class="jumbotron">
                        <div class="container">
                            <img src="../public/img/picnic/<?= $picnic['img'] ?>" width="150px" height="150px" alt="">
                            <h3><?= $picnic['name'] ?></h3>
                            <p><?= $picnic['details'] ?></p>
                            <p><?= $picnic['start'] ?>&nbsp;- <?= $picnic['stop'] ?></p>
                            <!-- Modal -->

                        </div>
                    </div>

                </div>

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <form method="POST">
                        <label for="id_student">Chọn học sinh</label> <br>
                        <select name="id_student" id="" class="btn btn-primary mb-5">
                            <?php
                                foreach (getData("SELECT * FROM student WHERE id_ph='$id'") as $name) { ?>
                                <option value="<?= $name['id'] ?>"><?= $name['name'] ?></option>
                            <?php

                                }
                                ?>
                        </select> <br> <br>
                        <input type="submit" name="submit" value="Lưu" class="btn btn-danger mt-5">
                    </form>
                </div>


            <?php

            }
            ?>
        </div>

        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>

</body>

</html>