<?php
include "header.php";
if (isset($_SESSION['admin'])) {
    $phone = $_SESSION['admin'];
    foreach (getData("SELECT * FROM admin WHERE phone = '$phone'") as $admin) {
        $name = $admin['name'];
        $id_admin = $admin['id'];
    }
}
if (isset($_POST['submit'])) {
    $date = Date('Y-m-d');
    $title = $_POST['title'];
    $content = $_POST['content'];
    $cate_id = $_POST['cate_id'];
    $img = $_FILES['img'];
    $maxSize = 800000;
    $upload = true;
    $dir = "../public/img/";
    $target_file = $dir . basename($img['name']);
    $type = pathinfo($target_file, PATHINFO_EXTENSION);
    $allowtypes = array('jpg', 'png', 'jpeg');
    if (empty($title) || empty($content) || empty($cate_id) || empty($date) || empty($img)) {
        $error = "Vui lòng điền đầy đủ thông tin";
    } elseif ($img["size"] > $maxSize) {
        $error = "File ảnh quá lớn. Vui lòng chọn ảnh khác";
        $upload = false;
    } elseif (!in_array($type, $allowtypes)) {
        $error = "Chỉ được upload các định dạng JPG, PNG, JPEG";
        $upload = false;
    } else {
        $imgname = uniqid() . "-" . $img['name'];
        if (move_uploaded_file($img['tmp_name'], $dir . $imgname)) { }

        $sql = "INSERT INTO post (title,content,id_cate,id_admin,img,date_add) VALUES 
        ('$title','$content','$cate_id','$id_admin','$imgname','$date')";
        action($sql);
        $error = "Thêm mới thành công";
    }
}
?>
<style>
    form input {
        width: 100%;
        height: 30px;
        border: 1px solid #cdcdcd;
        border-radius: 5px;
        margin: 10px 0;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thêm mới hoạt động</h1>
            </div>
        </div>
        <?php
        if (isset($error)) { ?>
            <p class="alert alert-danger"><?= $error ?></p>
        <?php
        }
        ?>
        <form method="post" enctype="multipart/form-data">
            <div class="row">

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="name">Tiêu đề</label> <br>
                    <input type="text" name="title"> <br>
                    <label for="cate_id">Nội dung</label> <br><br>
                    <select name="cate_id" id="">
                        <?php
                        foreach (getData("SELECT * FROM category") as $cate) { ?>
                            <option value="<?= $cate['id'] ?>"><?= $cate['name'] ?></option>
                        <?php

                        }

                        ?>
                    </select> <br><br>

                    <label>Hình ảnh</label>
                    <input id="img" type="file" name="img" class="form-control hidden" onchange="changeImg(this)">
                    <img id="avatar" class="thumbnail" width="300px" height="200px" src="../public/img/new.png">


                </div>

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="content">Nội dung</label>
                    <textarea name="content" id="" cols="30" rows="10" class="form-control"></textarea>


                </div>


            </div>
            <button type="submit" name="submit" class="btn btn-danger" style="margin-top: 10px;">Tạo mới</button>
            <a href="post.php" class="btn btn-primary" style="margin-top: 10px;">Quay lại</a>
        </form>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>
<script>
    function changeImg(input) {
        //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            //Sự kiện file đã được load vào website
            reader.onload = function(e) {
                //Thay đổi đường dẫn ảnh
                $('#avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function() {
        $('#avatar').click(function() {
            $('#img').click();
        });
    });
</script>

<script>
    function quay_lai_trang_truoc() {
        history.back();
    }
</script>
<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>
<!-- <script src="../public/js/flot/jquery.flot.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.pie.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.resize.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.time.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.tooltip.min.js"></script> -->
<!-- <script src="../public/js/flot-data.js"></script> -->

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>


</body>

</html>