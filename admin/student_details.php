<?php require_once "header.php";
// include("../function.php");
if (isset($_GET['id'])) {
    $id_student = $_GET['id'];
    foreach (getData("SELECT * FROM student WHERE id = '$id_student'") as $row) {
        $name = isset($row['name']) ? $row['name'] : '';
        $birthday = isset($row['birthday']) ? $row['birthday'] : '';
        $sex = isset($row['sex']) ? $row['sex'] : '';
        $address = isset($row['address']) ? $row['address'] : '';
        $phone = isset($row['phone']) ? $row['phone'] : '';
        $id_class = isset($row['id_class']) ? $row['id_class'] : '';
        $sabbatical_leave = isset($row['sabbatical_leave']) ? $row['sabbatical_leave'] : '';
        $img = isset($row['img']) ? $row['img'] : '';
        $id_ph = isset($row['id_ph']) ? $row['id_ph'] : '';
        $date_start_study = isset($row['date_start_study']) ? $row['date_start_study'] : '';
    }
}
?>
<style>
    form input {
        width: 100%;
        height: 30px;
        border: 1px solid #cdcdcd;
        border-radius: 5px;
    }

    form select {
        margin: 10px 0;
        height: 30px;
        width: 100px;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center" style="margin-top: 20px;">
                <h3 class="text-themecolor m-b-0 m-t-0">Thông tin học sinh</h3>
            </div>

        </div>
        <div class="row">
            <div class="card" style="margin-bottom: 30px;">
                <div class="card-block" style="text-align: center;">
                    <div class="m-t-30"> <img src="../public/img/student/<?= $img ?>" class="img-circle" width="150" />
                        <h4 class="card-title m-t-10"><?= $name ?></h4>

                    </div>
                </div>
                <form method="post">

                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="card-block">
                            <small class="text-muted p-t-30 db">Số điện thoại</small>
                            <input type="number" value="<?= $phone ?>">
                            <small class="text-muted p-t-30 db">Địa chỉ</small>
                            <input type="text" value="<?= $address ?>">
                            <small class="text-muted p-t-30 db">Ngày sinh</small>
                            <input type="text" value="<?= $birthday ?>">
                            <small class="text-muted p-t-30 db">Giới tính</small>
                            <select name="sex">
                                <option value="<?= $sex ?>"><?= $sex == 2 ? "Nữ" : "Nam" ?></option>
                            </select> <br>
                            <small class="text-muted p-t-30 db">Địa chỉ</small>
                            <input type="text" value="<?= $address ?>">
                            <small class="text-muted p-t-30 db">Số buổi nghỉ</small>
                            <input type="text" value="<?= $sabbatical_leave ?>">
                            <small class="text-muted p-t-30 db">Ngày bắt đầu đi học</small>
                            <input type="text" value="<?= $date_start_study ?>">


                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="card-block">

                            <small class="text-muted p-t-30 db">Học sinh lớp</small>
                            <?php
                            foreach (getData("SELECT * FROM class WHERE id = '$id_class'") as $class_name) { ?>
                                <input type="text" value="<?= $class_name['name'] ?>">
                            <?php

                            }
                            ?>
                            <?php
                            foreach (getData("SELECT * FROM parents WHERE id = '$id_ph'") as $parents) { ?>
                                <small class="text-muted p-t-30 db">Họ tên bố</small>
                                <input type="text" value="<?= $parents['name_father'] ?>">
                                <small class="text-muted p-t-30 db">Số điện thoại bố</small>
                                <input type="text" value="<?= $parents['phone_father'] ?>">
                                <small class="text-muted p-t-30 db">Họ tên mẹ</small>
                                <input type="text" value="<?= $parents['name_mother'] ?>">
                                <small class="text-muted p-t-30 db">Số điện thoại mẹ</small>
                                <input type="text" value="<?= $parents['phone_mother'] ?>">
                                <small class="text-muted p-t-30 db">Email</small>
                                <input type="text" value="<?= $parents['email'] ?>">
                            <?php
                            }
                            ?>

                        </div>
                    </div>


            </div>
            <div style="text-align: center;">
                <a href="student_edit.php?id_student=<?= $id_student ?>" class="btn btn-danger" style="margin:30px 0">Cập nhật</a>
                <a href="teacher_delete.php?id_student=<?= $id_student ?>&id_class=<?= $id_class ?>" class="btn btn-primary" style="margin:30px 0" onclick=" return confirm('Bạn chắc chắn giáo viên này nghỉ hoặc chấm dứt hợp đồng chứ?')">Nghỉ học</a>
                <a href="student.php?id=<?= $id_class ?>" class="btn btn-primary">Quay lại</a>
            </div>

            </form>

        </div>

    </div>
</div>

</div>
</div>
<script>
    function quay_lai_trang_truoc() {
        history.back();
    }
</script>
<script src="../public/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>

</body>

</html>