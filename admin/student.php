<?php

if (isset($_GET['id'])) {
    include "header.php";
    $id = $_GET['id'];
    $stt = 0;
    ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Quản trị học sinh lớp
                        <?php foreach (getData("SELECT * FROM class WHERE id ='$id'") as $row) { ?>
                            <?= $row['name'] ?></h1>
                <?php

                    } ?>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <a href="student_add.php?id=<?= $id ?>" class="btn btn-danger">Thêm mới học sinh</a>

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Stt</th>
                                <th>Hình ảnh</th>
                                <th>Tên</th>
                                <th>Ngày sinh</th>
                                <th>Quản trị</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                                foreach (getData("SELECT * FROM student WHERE id_class='$id'") as $row) { ?>
                                <tr>
                                    <td><?= $stt += 1 ?></td>
                                    <td><img src="../public/img/student/<?= $row['img'] ?>" alt="" width="100px" height="100px"></td>
                                    <td><?= $row['name'] ?></td>
                                    <td><?= $row['birthday'] ?></td>
                                    <td>
                                        <a href="student_details.php?id=<?= $row['id'] ?>" class="btn btn-primary">Xem chi tiết</a>
                                    </td>
                                </tr>
                            <?php

                                }





                                ?>


                        </tbody>
                    </table>

                </div>

            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../public/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../public/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../public/js/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../public/js/startmin.js"></script>

    </body>

    </html>
<?php
} else {
    header("Location:index.php");
}
?>