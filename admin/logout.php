<?php
session_start();
if (isset($_SESSION['teacher'])) {
	unset($_SESSION['teacher']);
	header("Location:../index.php");
} elseif (isset($_SESSION['admin'])) {
	unset($_SESSION['admin']);
	header("Location:../index.php");
} elseif (isset($_SESSION['parent'])) {
	unset($_SESSION['parent']);
	header("Location:../index.php");
}
?>
