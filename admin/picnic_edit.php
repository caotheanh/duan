<?php
include "header.php";
if (isset($_GET['id_picnic'])) {
    $id = $_GET['id_picnic'];
    foreach (getData("SELECT * FROM picnic WHERE id='$id'") as $picnic) {
        $name = isset($picnic['name']) ? $picnic['name'] : '';
        $start = isset($picnic['start']) ? $picnic['start'] : '';
        $stop = isset($picnic['stop']) ? $picnic['stop'] : '';
        $details = isset($picnic['details']) ? $picnic['details'] : '';
        $img = isset($picnic['img']) ? $picnic['img'] : '';
    }
};
if (isset($_POST['submit'])) {
    $name_new = $_POST['name'];
    $start_new = $_POST['start'];
    $stop_new = $_POST['stop'];
    $details_new = $_POST['details'];
    if (isset($_FILES['img']) && $_FILES['img']['name']) {
        $img_new = $_FILES['img'];
        $maxSize = 800000;
        $upload = true;
        $dir = "../public/img/picnic/";
        $target_file = $dir . basename($img_new['name']);
        $type = pathinfo($target_file, PATHINFO_EXTENSION);
        $allowtypes = array('jpg', 'png', 'jpeg');
        if ($img_new["size"] > $maxSize) {
            $error = "File ảnh quá lớn. Vui lòng chọn ảnh khác";
            $upload = false;
        } elseif (!in_array($type, $allowtypes)) {
            $error = "Chỉ được upload các định dạng JPG, PNG, JPEG";
            $upload = false;
        } else {
            $imgname = uniqid() . "-" . $img_new['name'];
            move_uploaded_file($img_new['tmp_name'], $dir . $imgname);
            action("UPDATE picnic SET name= '$name_new',start='$start_new',stop='$stop_new',details='$details_new',img= '$imgname' WHERE id = '$id' ");
            $error = "Cập nhật thành công";
        }
    } else {
        action("UPDATE picnic SET name= '$name_new',start='$start_new',stop='$stop_new',details='$details_new' WHERE id = '$id' ");
        $error = "Cập nhật thành công";
    }
}
// action("UPDATE picnic SET name= '$name_new','start'='$start_new',stop='$stop_new',details='$details_new',img= '$imgname' WHERE id = '$id' ");
?>
<style>
    form input {
        width: 100%;
        height: 30px;
        border: 1px solid #cdcdcd;
        border-radius: 5px;
        margin: 10px 0;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thêm mới hoạt động</h1>
            </div>
        </div>
        <?php
        if (isset($error)) { ?>
            <p class="alert alert-danger"><?= $error ?></p>
        <?php


        }
        ?>
        <form method="post" enctype="multipart/form-data">
            <div class="row">

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="name">Tên hoạt động</label> <br>
                    <input type="text" name="name" value="<?= $name ?>"> <br>
                    <label for="birthday">Ngày bắt đầu</label> <br>
                    <input type="text" name="start" value="<?= $start ?>"> <br>

                    <label for="stop">Ngày kết thúc</label> <br>
                    <input type="text" name="stop" value="<?= $stop ?>"> <br>
                    <label>Hình ảnh</label>
                    <input id="img" type="file" name="img" class="form-control hidden" onchange="changeImg(this)">
                    <img id="avatar" class="thumbnail" width="300px" height="200px" src="../public/img/picnic/<?= $img ?>">


                </div>

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label for="details">Mô tả chi tiết</label>
                    <textarea name="details" id="" cols="30" rows="10" class="form-control"><?= $details ?></textarea>


                </div>


            </div>
            <button type="submit" name="submit" class="btn btn-danger" style="margin-top: 10px;">Cập nhật</button>
            <a href="picnic.php" class="btn btn-primary" style="margin-top: 10px;">Quay lại</a>
        </form>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../public/js/jquery.min.js"></script>
<script>
    function changeImg(input) {
        //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            //Sự kiện file đã được load vào website
            reader.onload = function(e) {
                //Thay đổi đường dẫn ảnh
                $('#avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function() {
        $('#avatar').click(function() {
            $('#img').click();
        });
    });
</script>

<script>
    function quay_lai_trang_truoc() {
        history.back();
    }
</script>
<!-- Bootstrap Core JavaScript -->
<script src="../public/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../public/js/metisMenu.min.js"></script>

<!-- Flot Charts JavaScript -->
<script src="../public/js/flot/excanvas.min.js"></script>
<!-- <script src="../public/js/flot/jquery.flot.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.pie.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.resize.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.time.js"></script> -->
<!-- <script src="../public/js/flot/jquery.flot.tooltip.min.js"></script> -->
<!-- <script src="../public/js/flot-data.js"></script> -->

<!-- Custom Theme JavaScript -->
<script src="../public/js/startmin.js"></script>


</body>

</html>