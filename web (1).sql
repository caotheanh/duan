-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 08, 2019 at 03:31 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `phone`, `address`, `password`) VALUES
(1, 'Anh', 'anh@gmail.com', 379723021, 'hn', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_gr` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`id`, `name`, `id_gr`) VALUES
(44, '2T-A1', 1),
(45, '2T-A2', 1),
(46, '3T-A1', 2),
(47, '3T-A2', 2),
(48, '4T-A1', 3),
(49, '4T-A2', 3),
(50, '5T-A1', 4),
(51, '5T-A2', 4);

-- --------------------------------------------------------

--
-- Table structure for table `details_picnic`
--

CREATE TABLE `details_picnic` (
  `id_picnic` int(11) NOT NULL,
  `id_hs` int(11) NOT NULL,
  `start` varchar(255) DEFAULT NULL,
  `stop` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `group_class`
--

CREATE TABLE `group_class` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_class`
--

INSERT INTO `group_class` (`id`, `name`) VALUES
(1, 'Lớp 2 tuổi'),
(2, 'Lớp 3 tuổi'),
(3, 'Lớp 4 tuổi'),
(4, 'Lớp 5 tuổi');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `Mon` varchar(255) NOT NULL,
  `Tue` varchar(255) NOT NULL,
  `Wed` varchar(255) NOT NULL,
  `Thu` varchar(255) NOT NULL,
  `Fri` varchar(255) NOT NULL,
  `week` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `Mon`, `Tue`, `Wed`, `Thu`, `Fri`, `week`) VALUES
(1, 'Canh cá, thịt băm, rau cải', 'Cá', 'Thịt bò', 'Thịt chó mắm tôm', 'Thịt sư tử', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu_admin`
--

CREATE TABLE `menu_admin` (
  `id` int(11) NOT NULL,
  `number_student` int(11) NOT NULL,
  `id_class` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_admin`
--

INSERT INTO `menu_admin` (`id`, `number_student`, `id_class`) VALUES
(1, 2, 44);

-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

CREATE TABLE `parents` (
  `id` int(11) NOT NULL,
  `name_mother` varchar(255) NOT NULL,
  `birthday_mother` varchar(255) NOT NULL,
  `phone_mother` varchar(11) NOT NULL,
  `address_mother` varchar(255) NOT NULL,
  `name_father` varchar(255) DEFAULT NULL,
  `birthday_father` varchar(255) DEFAULT NULL,
  `phone_father` varchar(11) DEFAULT NULL,
  `address_father` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parents`
--

INSERT INTO `parents` (`id`, `name_mother`, `birthday_mother`, `phone_mother`, `address_mother`, `name_father`, `birthday_father`, `phone_father`, `address_father`, `email`, `password`) VALUES
(1, 'Anh', '1970', '123523', 'hn', 'anh', '1970', '123321', 'hn', 'anh@gmail.com', '123');

-- --------------------------------------------------------

--
-- Table structure for table `picnic`
--

CREATE TABLE `picnic` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `start` varchar(255) NOT NULL,
  `stop` varchar(255) NOT NULL,
  `id_ph` int(11) NOT NULL,
  `id_student` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `id_cate` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `date_add` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE `request` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `sex` tinyint(2) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `id_class` int(11) NOT NULL,
  `date_start_study` varchar(255) DEFAULT NULL,
  `sabbatical_leave` int(11) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `id_ph` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `birthday`, `sex`, `address`, `phone`, `id_class`, `date_start_study`, `sabbatical_leave`, `img`, `id_ph`) VALUES
(6, 'Cao Thế Anh', '23/02/1997', 1, 'Ha Noi', 1234567890, 44, '2019-11-24', 0, '5ddaca771c7b8-18582017_701410053377331_2378200943485044137_n.jpg', 1),
(7, 'the anh cao', '23/02/1997', 1, 'Ha Noi', 1234567899, 44, '2019-12-07', 0, '5debeb43dc6ec-pic2.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `sex` tinyint(4) NOT NULL,
  `id_class` int(11) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `education` varchar(255) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `start_work` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id`, `name`, `birthday`, `sex`, `id_class`, `phone`, `education`, `img`, `password`, `address`, `email`, `start_work`) VALUES
(16, 'the anh cao', '23/02/1997', 2, 44, '1234567890', 'FPT', '5dda9dbca2c4f-18582017_701410053377331_2378200943485044137_n.jpg', 'c4ca4238a0b923820dcc509a6f75849b', 'Ha Noi', 'anhct.poly@gmail.com', '2019-12-03'),
(17, 'the anh cao1111', '23/02/1997', 2, 44, '1234567891', 'FPT', '5de7491227f50-28685198_843790605805941_5762572979846250496_n.jpg', '202cb962ac59075b964b07152d234b70', 'Ha Noi', 'anhct.poly1@gmail.com', '2019-12-04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_gr` (`id_gr`);

--
-- Indexes for table `details_picnic`
--
ALTER TABLE `details_picnic`
  ADD PRIMARY KEY (`id_picnic`,`id_hs`) USING BTREE,
  ADD KEY `id_hs` (`id_hs`);

--
-- Indexes for table `group_class`
--
ALTER TABLE `group_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_admin`
--
ALTER TABLE `menu_admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_class` (`id_class`);

--
-- Indexes for table `parents`
--
ALTER TABLE `parents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `picnic`
--
ALTER TABLE `picnic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cate` (`id_cate`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_lop` (`id_class`),
  ADD KEY `id_ph` (`id_ph`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_class` (`id_class`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `group_class`
--
ALTER TABLE `group_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_admin`
--
ALTER TABLE `menu_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `parents`
--
ALTER TABLE `parents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `picnic`
--
ALTER TABLE `picnic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `class`
--
ALTER TABLE `class`
  ADD CONSTRAINT `class_ibfk_1` FOREIGN KEY (`id_gr`) REFERENCES `group_class` (`id`);

--
-- Constraints for table `details_picnic`
--
ALTER TABLE `details_picnic`
  ADD CONSTRAINT `details_picnic_ibfk_1` FOREIGN KEY (`id_hs`) REFERENCES `student` (`id`),
  ADD CONSTRAINT `details_picnic_ibfk_2` FOREIGN KEY (`id_picnic`) REFERENCES `picnic` (`id`);

--
-- Constraints for table `menu_admin`
--
ALTER TABLE `menu_admin`
  ADD CONSTRAINT `menu_admin_ibfk_1` FOREIGN KEY (`id_class`) REFERENCES `class` (`id`);

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`id_cate`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `post_ibfk_2` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id`);

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `student_ibfk_1` FOREIGN KEY (`id_class`) REFERENCES `class` (`id`),
  ADD CONSTRAINT `student_ibfk_3` FOREIGN KEY (`id_ph`) REFERENCES `parents` (`id`);

--
-- Constraints for table `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `teacher_ibfk_1` FOREIGN KEY (`id_class`) REFERENCES `class` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
