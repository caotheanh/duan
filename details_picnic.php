<?php
include "header.php";
if (isset($_GET['id_picnic'])) {
    $id = $_GET['id_picnic'];
}
?>
<!-- Blog -->
<link rel="stylesheet" type="text/css" href="styles/blog.css">

<div class="blog" style="height: 1000px;margin-top: 50px;">
    <div class="container">
        <div class="row">

            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <h3 style="margin: 10px;">Các hoạt động liên quan</h3>
                <?php
                foreach (getData("SELECT * FROM picnic ORDER BY RAND()") as $name) { ?>
                    <div style="border: 1px solid #cdcdcd;padding-left: 10px;" class="p-3">
                        <a href="details_picnic.php?id_picnic=<?= $name['id'] ?>"><?= $name['name'] ?></a>
                    </div>
                <?php
                }

                ?>
            </div>

            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                <?php
                foreach (getData("SELECT * FROM picnic WHERE id = '$id'") as $picnic) { ?>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 15px;margin-bottom: 20px;">

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                <a href="details_picnic.php?id_picnic=<?= $picnic['id'] ?>" style="font-size: 25px;text-align: center;"><?= $picnic['name'] ?></a>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center mt-4">
                                <img src="public/img/picnic/<?= $picnic['img'] ?>" width="100%" height="400px" alt="">
                                <p style="font-size: 13px;font-style: italic;">Thời gian: <?= $picnic['start'] ?> - <?= $picnic['stop'] ?></p>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                <p style="line-height: 30px;"><?= $picnic['details'] ?></p>
                            </div>

                        </div>
                        <div class="text-center m-4">
                            <a href="
                            <?php
                                if(isset($_SESSION['parent'])){
                                    echo "admin/register_picnic.php";
                                }else{
                                    echo "login.php";
                                }
                            ?>
                            
                            "
                             class="btn btn-info">Đăng ký ngay</a>
                             <!-- admin/register_picnic.php -->
                        </div>
                    </div>
                <?php

                }

                ?>
            </div>


        </div>

    </div>
</div>

<?php
include "footer.php";
?>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/blog.js"></script>