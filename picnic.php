<?php
include "header.php";
?>
<!-- Blog -->
<link rel="stylesheet" type="text/css" href="styles/blog.css">

<div class="blog" style="height: 1000px;margin-top: 50px;">
    <div class="text-center m-3">
        <h2>Danh sách các hoạt động ngoại khóa sắp diễn ra</h2>
    </div>
    <div class="container">
        <div class="row">



            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php
                foreach (getData("SELECT * FROM picnic") as $picnic) { ?>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="border: 1px solid #cdcdcd;padding:
                     15px;margin-bottom: 20px;overflow: hidden;border-radius: 10px;">

                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <img src="public/img/picnic/<?= $picnic['img'] ?>" width="150px" height="150px" alt="">
                            </div>

                            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                <a href="details_picnic.php?id_picnic=<?= $picnic['id'] ?>"><?= $picnic['name'] ?></a>
                                <p><?= substr($picnic['details'], 0, 200)  ?>...</p>
                            </div>

                        </div>

                    </div>
                <?php

                }

                ?>
            </div>


        </div>

    </div>
</div>

<?php
include "footer.php";
?>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/blog.js"></script>