<?php
include "header.php";
if (isset($_GET['id'])) {
    $id = $_GET['id'];
}
?>
<!-- Blog -->
<link rel="stylesheet" type="text/css" href="styles/blog.css">

<div class="blog" style="height: 100%;margin-top: 50px;">
    <div class="container">
        <div class="row">

            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <h3 class="mb-3">Bài viết liên quan</h3>
                <?php
                foreach (getData("SELECT * FROM post ORDER BY RAND() ") as $post) { ?>
                    <div style="border: 1px solid #cdcdcd;padding: 15px;">
                        <a href="details_post.php?id=<?= $post['id'] ?>"><?= $post['title'] ?></a>
                    </div>
                <?php
                }

                ?>
            </div>

            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                <?php
                foreach (getData("SELECT * FROM post WHERE id = '$id'") as $post) { ?>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 15px;margin-bottom: 20px;">

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                <a href="details_post.php?id=<?= $post['id'] ?>" style="font-size: 25px;text-align: center;"><?= $post['title'] ?></a>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center mt-4">
                                <img src="public/img/<?= $post['img'] ?>" width="100%" height="400px" alt="">
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-3">
                                <p style="line-height: 30px;"><?= $post['content'] ?></p>
                            </div>

                            <?php
                                foreach (getData("SELECT * FROM admin WHERE id =" . $post['id_admin']) as $name) { ?>
                                <div style="font-style:italic ;width: 100%;" class="text-right mt-5">
                                    <p>Tác giả: <?= $name['name'] ?></p>
                                </div>
                            <?php

                                }
                                ?>

                        </div>

                    </div>
                <?php

                }

                ?>
            </div>


        </div>

    </div>
</div>

<?php
include "footer.php";
?>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/blog.js"></script>