
<?php
session_start();
include "function.php" ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Unicat</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="Unicat project">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
	<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
	<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
	<link rel="stylesheet" type="text/css" href="styles/main_styles.css">
	<link rel="stylesheet" type="text/css" href="styles/responsive.css">
</head>

<body>

	<div class="super_container">

		<!-- Header -->

		<header class="header">

			<!-- Top Bar -->


			<!-- Header Content -->
			<div class="header_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="header_content d-flex flex-row align-items-center justify-content-start">
								<div class="logo_container">
									<a href="index.php">
										<div class="logo_text">AM<span>S</span></div>
									</a>
								</div>
								<nav class="main_nav_contaner ml-auto">
									<ul class="main_nav">
										<li><a href="index.php">Trang chủ</a></li>
										<li><a href="about.php">Giới thiệu</a></li>
										<!-- <li><a href="courses.php">Courses</a></li> -->
										<li><a href="blog.php">Tin tức</a></li>
										<!-- <li><a href="#">Page</a></li> -->
										<li><a href="contact.php">Liên hệ</a></li>
										<?php
										if (isset($_SESSION['parent'])) { ?>
											<li><?= $_SESSION['parent'] ?></li>
										<?php

										} else { ?>
											<li><a href="login.php">Đăng nhập</a></li>
										<?php

										}
										?>
									</ul>

								</nav>

							</div>
						</div>
					</div>
				</div>
			</div>
		</header>