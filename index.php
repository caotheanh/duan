<?php
include "header.php";
if (isset($_POST['submit'])) {
	$name = $_POST['name'];
	$phone = $_POST['phone'];
	$details = $_POST['details'];
	$status = 0;
	action("INSERT INTO tuvan(name,phone,content,status) VALUES ('$name','$phone','$details','$status')");
	$error = "Gửi thành công";
}
?>
<style>
	#slide_id {
		position: relative;
	}

	.title_slide {
		text-align: center;
		position: absolute;
		top: 150px;
		left: 35%;
	}
</style>
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner">
		<?php
		foreach (getData("SELECT * FROM slide WHERE status = 1") as $slide) { ?>
			<div class="carousel-item active" id="slide_id">
				<a href="<?= $slide['link'] ?>">
					<img src="public/img/slide/<?= $slide['img'] ?>" class="d-block w-100" alt="...">
					<div class="title_slide">
						<h3 class="text-center"><?= $slide['title'] ?></h3>
					</div>
				</a>
			</div>
		<?php

		}

		?>
	</div>
	<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
<div class="features">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="section_title_container text-center">
					<h2 class="section_title">Chào mừng đến với trường mầm non Ánh Mai Sáng</h2>

				</div>
			</div>
		</div>

	</div>
</div>

<!-- Popular Courses -->



<!-- Counter -->

<div class="counter">
	<div class="counter_background" style="background-image:url(images/counter_background.jpg)"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="counter_content">
					<h2 class="counter_title">Đăng ký học ngay hôm nay</h2>
					<div class="counter_text">
						<p>Với môi trường học tập được chuẩn bị một cách toàn diện, lý tưởng, cơ
							sở vật chất hiện đại tiêu chuẩn quốc tế Trường Mầm non
							AMS sẽ là một trải nghiệm tuyệt vời dành cho các bé từ 6 tháng cho đến
							6 tuổi.</p>
					</div>

					<!-- Milestones -->

					<div class="milestones d-flex flex-md-row flex-column align-items-center justify-content-between">
						<?php
							$class = total("SELECT COUNT(*) FROM class");
							$teacher = total("SELECT COUNT(*) FROM teacher");
							$student = total("SELECT COUNT(*) FROM student");
						?>
						<!-- Milestone -->
						<div class="milestone">
							<div class="milestone_counter" data-end-value="<?=$class ?>">0</div>
							<div class="milestone_text">Lớp</div>
						</div>

						<!-- Milestone -->
						<div class="milestone">
							<div class="milestone_counter" data-end-value="<?=$teacher ?>">0</div>
							<div class="milestone_text">Giáo viên</div>
						</div>

						<!-- Milestone -->
						<div class="milestone">
							<div class="milestone_counter" data-end-value="<?=$student ?>">0</div>
							<div class="milestone_text">Học sinh</div>
						</div>


					</div>
				</div>

			</div>
		</div>

		<div class="counter_form">
			<div class="row fill_height">
				<div class="col fill_height">
					<?php
					if (isset($error)) { ?>
						<p class="alert alert-danger fomr-control"><?= $error ?></p>
					<?php

					}

					?>
					<form class="counter_form_content d-flex flex-column align-items-center justify-content-center" method="POST">

						<div class="counter_form_title">Đăng ký tư vấn</div>
						<input type="text" class="counter_input" placeholder="Tên của bạn:" required="required" name="name">
						<input type="tel" class="counter_input" placeholder="Số điện thoại" : required="required" name="phone">

						<textarea class="counter_input counter_text_input" placeholder="Để lại lời nhắn:" required="required" name="details"></textarea>
						<button type="submit" name="submit" class="counter_form_button">Gửi</button>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>

<!-- Events -->

<div class="events">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="section_title_container text-center">
					<h2 class="section_title">Các hoạt động ngoại khóa nổi bật</h2>
					<div class="section_subtitle">
						<p>Để có thể đăng ký được hoạt động, các bậc phụ huynh vui lòng đăng nhập.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row events_row">

			<!-- Event -->
			<?php
			foreach (getData("SELECT * FROM picnic ORDER BY id DESC limit 3") as $piclic) { ?>
				<div class="col-lg-4 event_col">
					<div class="event event_left">
						<div class="event_image"><img src="public/img/picnic/<?= $piclic['img'] ?>" width="100%" height="100%" alt=""></div>
						<div class="event_body d-flex flex-row align-items-start justify-content-start">
							<div class="event_date">

							</div>
							<div class="event_content">
								<div class="event_title"><a href="details_picnic.php?id_picnic=<?= $piclic['id'] ?>"><?= $piclic['name'] ?></a>
								</div>
								<div class="event_info_container">
									<div class="event_info"><i class="fa fa-clock-o" aria-hidden="true"></i><span><?= $piclic['start'] ?>- <?= $piclic['stop'] ?></span></div>
									<div class="event_text">
										<p><?= substr($piclic['details'], 0, 100)  ?>...</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php

			}
			?>
			<div style="text-align: center;">
				<a href="picnic.php" style="margin: 0 auto;">Xem thêm >></a>
			</div>
		</div>
	</div>
</div>







<!-- Footer -->
<?php include "footer.php" ?>