<?php
require_once "header.php"
?>
<div class="container" style="margin-top: 100px;">
	<div class="row">

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="section_title_container text-center">
				<h2 class="section_title">Chào mừng đến với trường mầm non Ánh Mai Sáng</h2>
			</div>
		</div>


		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
			<div class="about_item">
				<div class="about_item_image"><img src="images/about_1.jpg" alt="" class="img-fluid"></div>
				<div class="about_item_title" style="margin-top: 20px;"><a href="#">Giáo viên được đào tạo bài bản</a></div>
				<div class="about_item_text">
					<p>Ở Ánh Mai Sáng chúng tôi tự tin có những giáo viên chất lượng nhất được đào tạo bài bản trong nước và nước ngoài.</p>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="about_item">
				<div class="about_item_image"><img src="images/about_2.jpg" alt="" class="img-fluid"></div>
				<div class="about_item_title" style="margin-top: 20px;"><a href="#">Liên kết thường xuyên giữa gia đình mà nhà trường</a></div>
				<div class="about_item_text">
					<p>Sau mỗi 6 tháng, nhà trường sẽ tổ chức cuộc họp phụ huynh để thông báo tình hình con trẻ cũng như giới thiệu nhiều
						hoạt động đặc biệt giúp nhà phụ huynh kiểm soát tốt nhất
						sự phát triển của con em mình.</p>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="about_item">
				<div class="about_item_image"><img src="images/about_3.jpg" alt="" class="img-fluid"></div>
				<div class="about_item_title" style="margin-top: 20px;"><a href="#">Bố mẹ yên tâm đi làm, Công tác</a></div>
				<div class="about_item_text">
					<p>
						Nhà trường linh hoạt trong việc chăm sóc con trẻ, cơ sở vật chất đầy đủ, có các phòng ban phục vụ nhu cầu của trẻ như:
						phòng vui chơi, phòng y tế, phòng phát triển tư duy,...
					</p>
				</div>
			</div>
		</div>

	</div>
</div>
<div class="feature" style="margin-top: 50px;">
	<div class="feature_background" style="background-image:url(images/courses_background.jpg)"></div>
	<div class="container">
		<div class="row">
			<div class="col">
				<div class=" text-center">
					<h2 class="section_title" style="margin-bottom: 30px;">Tại sao chọn Ánh Mai Sáng</h2>
					<?php
					foreach (getData("SELECT why FROM info") as $row) { ?>
						<p><?=$row['why'] ?></p>
					<?php

					}

					?>

				</div>
			</div>
		</div>

	</div>
</div>






<?php
require_once "footer.php";
?>