<footer class="footer">
	<div class="footer_background" style="background-image:url(images/footer_background.png)"></div>
	<div class="container">
		<div class="row footer_row">
			<div class="col">
				<div class="footer_content">
					<div class="row text-center">



						<div class="col-lg-12 footer_col">

							<!-- Footer Contact -->
							<div class="footer_section footer_contact">
								<div class="footer_title">Contact Us</div>
								<div class="footer_contact_info">
									<ul>
										<?php
										foreach (getData("SELECT * FROM info") as $info) { ?>
											<li>Email: <?=$info['email'] ?></li>
											<li>Phone: <?=$info['phone'] ?></li>
											<li><?=$info['address'] ?></li>
										<?php

										}

										?>

									</ul>
								</div>
							</div>

						</div>





					</div>
				</div>
			</div>
		</div>

	</div>
</footer>
</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>
</body>

</html>